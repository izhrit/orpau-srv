-- MySQL dump 10.13  Distrib 5.7.28, for Win64 (x86_64)
--
-- Host: localhost    Database: orpaudevel
-- ------------------------------------------------------
-- Server version	5.7.28-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `access_log`
--

DROP TABLE IF EXISTS `access_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `access_log` (
  `id_Log` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Log_type` int(11) NOT NULL,
  `IP` varchar(40) DEFAULT NULL,
  `INN` varchar(12) DEFAULT NULL,
  `OGRN` varchar(15) DEFAULT NULL,
  `SNILS` varchar(11) DEFAULT NULL,
  `Time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Details` text,
  PRIMARY KEY (`id_Log`),
  KEY `byTime` (`Time`),
  KEY `byINN` (`INN`),
  KEY `bySNILS` (`SNILS`),
  KEY `by_log_type` (`id_Log_type`),
  CONSTRAINT `by_log_type` FOREIGN KEY (`id_Log_type`) REFERENCES `log_type` (`id_Log_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `access_log`
--

LOCK TABLES `access_log` WRITE;
/*!40000 ALTER TABLE `access_log` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `access_log` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `confirmation`
--

DROP TABLE IF EXISTS `confirmation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `confirmation` (
  `id_Confirmation` int(11) NOT NULL AUTO_INCREMENT,
  `id_Manager` int(11) NOT NULL,
  `id_Debtor` int(11) NOT NULL,
  `Number` varchar(20) NOT NULL,
  `TimeOfCreation` datetime NOT NULL,
  `Text` longblob NOT NULL,
  `Signature` longblob,
  `EfrsbMessageNumber` varchar(15) DEFAULT NULL,
  `JudicalActId` varchar(15) DEFAULT NULL,
  `CaseNumber` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id_Confirmation`),
  UNIQUE KEY `byNumber` (`Number`),
  KEY `Ref_Confirmation_Manager` (`id_Manager`),
  KEY `Ref_Confirmation_Debtor` (`id_Debtor`),
  CONSTRAINT `Ref_Confirmation_Debtor` FOREIGN KEY (`id_Debtor`) REFERENCES `debtor` (`id_Debtor`),
  CONSTRAINT `Ref_Confirmation_Manager` FOREIGN KEY (`id_Manager`) REFERENCES `manager` (`id_Manager`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `confirmation`
--

LOCK TABLES `confirmation` WRITE;
/*!40000 ALTER TABLE `confirmation` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `confirmation` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `debtor`
--

DROP TABLE IF EXISTS `debtor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `debtor` (
  `id_Debtor` int(11) NOT NULL AUTO_INCREMENT,
  `BankruptId` bigint(20) DEFAULT NULL,
  `ArbitrManagerID` int(11) DEFAULT NULL,
  `id_Region` int(11) DEFAULT NULL,
  `INN` varchar(12) DEFAULT NULL,
  `SNILS` varchar(11) DEFAULT NULL,
  `Name` varchar(152) DEFAULT NULL,
  `OGRN` varchar(15) DEFAULT NULL,
  `Revision` bigint(20) DEFAULT NULL,
  `LegalAdress` varchar(400) DEFAULT NULL,
  `LegalCaseList` longblob,
  PRIMARY KEY (`id_Debtor`),
  UNIQUE KEY `byBankruptId` (`BankruptId`),
  KEY `byName` (`Name`),
  KEY `byINN` (`INN`),
  KEY `bySNILS` (`SNILS`),
  KEY `byOGRN` (`OGRN`),
  KEY `byRevision` (`Revision`),
  KEY `Ref_Debtor_Manager` (`ArbitrManagerID`),
  KEY `refDebtor_Region` (`id_Region`),
  CONSTRAINT `refDebtor_Region` FOREIGN KEY (`id_Region`) REFERENCES `region` (`id_Region`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `debtor`
--

LOCK TABLES `debtor` WRITE;
/*!40000 ALTER TABLE `debtor` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `debtor` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `debtor_manager`
--

DROP TABLE IF EXISTS `debtor_manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `debtor_manager` (
  `id_Debtor_Manager` int(11) NOT NULL,
  `BankruptId` bigint(20) DEFAULT NULL,
  `ArbitrManagerID` int(11) DEFAULT NULL,
  `DateTime_MessageFirst` datetime DEFAULT NULL,
  `DateTime_MessageLast` datetime DEFAULT NULL,
  `Revision` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_Debtor_Manager`),
  KEY `Ref_Debtor_manager_Debtor` (`BankruptId`),
  KEY `Ref_Debtor_manager_Manager` (`ArbitrManagerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `debtor_manager`
--

LOCK TABLES `debtor_manager` WRITE;
/*!40000 ALTER TABLE `debtor_manager` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `debtor_manager` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `email_attachment`
--

DROP TABLE IF EXISTS `email_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_attachment` (
  `id_Email_Attachment` int(11) NOT NULL AUTO_INCREMENT,
  `id_Email_Message` int(11) NOT NULL,
  `FileName` varchar(50) DEFAULT NULL,
  `Content` longblob,
  PRIMARY KEY (`id_Email_Attachment`),
  KEY `Email_Message_to_Attachment` (`id_Email_Message`),
  CONSTRAINT `Email_Message_to_Attachment` FOREIGN KEY (`id_Email_Message`) REFERENCES `email_message` (`id_Email_Message`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_attachment`
--

LOCK TABLES `email_attachment` WRITE;
/*!40000 ALTER TABLE `email_attachment` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `email_attachment` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `email_error`
--

DROP TABLE IF EXISTS `email_error`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_error` (
  `id_Email_Error` int(11) NOT NULL AUTO_INCREMENT,
  `id_Email_Message` int(11) NOT NULL,
  `id_Email_Sender` int(11) NOT NULL,
  `TimeError` datetime NOT NULL,
  `Description` text NOT NULL,
  PRIMARY KEY (`id_Email_Error`),
  KEY `Email_Message_Error` (`id_Email_Message`),
  KEY `Email_Sender_Error` (`id_Email_Sender`),
  CONSTRAINT `Email_Message_Error` FOREIGN KEY (`id_Email_Message`) REFERENCES `email_message` (`id_Email_Message`),
  CONSTRAINT `Email_Sender_Error` FOREIGN KEY (`id_Email_Sender`) REFERENCES `email_sender` (`id_Email_Sender`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_error`
--

LOCK TABLES `email_error` WRITE;
/*!40000 ALTER TABLE `email_error` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `email_error` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `email_message`
--

DROP TABLE IF EXISTS `email_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_message` (
  `id_Email_Message` int(11) NOT NULL AUTO_INCREMENT,
  `RecipientEmail` varchar(100) NOT NULL,
  `RecipientId` int(11) NOT NULL,
  `RecipientType` char(1) NOT NULL,
  `EmailType` char(1) NOT NULL,
  `TimeDispatch` datetime NOT NULL,
  `Details` longblob,
  PRIMARY KEY (`id_Email_Message`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_message`
--

LOCK TABLES `email_message` WRITE;
/*!40000 ALTER TABLE `email_message` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `email_message` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `email_sender`
--

DROP TABLE IF EXISTS `email_sender`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_sender` (
  `id_Email_Sender` int(11) NOT NULL AUTO_INCREMENT,
  `SenderServer` varchar(50) DEFAULT NULL,
  `SenderUser` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_Email_Sender`),
  UNIQUE KEY `byServerUser` (`SenderServer`,`SenderUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_sender`
--

LOCK TABLES `email_sender` WRITE;
/*!40000 ALTER TABLE `email_sender` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `email_sender` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `email_sent`
--

DROP TABLE IF EXISTS `email_sent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_sent` (
  `id_Email_sent` int(11) NOT NULL AUTO_INCREMENT,
  `id_Email_Message` int(11) NOT NULL,
  `id_Email_Sender` int(11) NOT NULL,
  `TimeSent` datetime NOT NULL,
  `Message` longblob,
  PRIMARY KEY (`id_Email_sent`),
  KEY `Email_Message_Sent` (`id_Email_Message`),
  KEY `Email_Sender_Sent` (`id_Email_Sender`),
  CONSTRAINT `Email_Message_Sent` FOREIGN KEY (`id_Email_Message`) REFERENCES `email_message` (`id_Email_Message`),
  CONSTRAINT `Email_Sender_Sent` FOREIGN KEY (`id_Email_Sender`) REFERENCES `email_sender` (`id_Email_Sender`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_sent`
--

LOCK TABLES `email_sent` WRITE;
/*!40000 ALTER TABLE `email_sent` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `email_sent` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `email_tosend`
--

DROP TABLE IF EXISTS `email_tosend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_tosend` (
  `TimeDispatch` datetime NOT NULL,
  `id_Email_Message` int(11) NOT NULL,
  KEY `byTimeDispatch` (`TimeDispatch`,`id_Email_Message`),
  KEY `Email_Message_ToSend` (`id_Email_Message`),
  CONSTRAINT `Email_Message_ToSend` FOREIGN KEY (`id_Email_Message`) REFERENCES `email_message` (`id_Email_Message`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_tosend`
--

LOCK TABLES `email_tosend` WRITE;
/*!40000 ALTER TABLE `email_tosend` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `email_tosend` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `log_type`
--

DROP TABLE IF EXISTS `log_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_type` (
  `id_Log_type` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) NOT NULL,
  PRIMARY KEY (`id_Log_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_type`
--

LOCK TABLES `log_type` WRITE;
/*!40000 ALTER TABLE `log_type` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `log_type` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `manager`
--

DROP TABLE IF EXISTS `manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager` (
  `id_Manager` int(11) NOT NULL AUTO_INCREMENT,
  `ArbitrManagerID` int(11) NOT NULL,
  `SRORegNum` varchar(30) DEFAULT NULL,
  `id_Region` int(11) DEFAULT NULL,
  `FirstName` varchar(50) NOT NULL,
  `MiddleName` varchar(50) DEFAULT NULL,
  `LastName` varchar(50) NOT NULL,
  `OGRNIP` varchar(15) DEFAULT NULL,
  `INN` varchar(12) NOT NULL,
  `SRORegDate` datetime DEFAULT NULL,
  `RegNum` varchar(30) DEFAULT NULL,
  `Revision` bigint(20) NOT NULL,
  `CorrespondenceAddress` varchar(300) DEFAULT NULL,
  `SNILS` varchar(11) DEFAULT NULL,
  `Phone` varchar(12) DEFAULT NULL,
  `EMail` varchar(100) DEFAULT NULL,
  `Password` varchar(100) DEFAULT NULL,
  `Salt` varchar(50) DEFAULT NULL,
  `Login` varchar(100) DEFAULT NULL,
  `AgreementText` longblob,
  `AgreementSignature` longblob,
  `ClubAgreementText` longblob,
  `ClubAgreementSignature` longblob,
  `PhoneInAgreement` varchar(12) DEFAULT NULL,
  `ConfirmationCode` varchar(10) DEFAULT NULL,
  `ConfirmationCodeTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id_Manager`),
  UNIQUE KEY `byArbitrManagerID` (`ArbitrManagerID`),
  KEY `byLastName` (`LastName`),
  KEY `byFirstName` (`FirstName`),
  KEY `byMiddleName` (`MiddleName`),
  KEY `byINN` (`INN`),
  KEY `byRevision` (`Revision`),
  KEY `refManager_SRO` (`SRORegNum`),
  KEY `refManager_Region` (`id_Region`),
  CONSTRAINT `refManager_Region` FOREIGN KEY (`id_Region`) REFERENCES `region` (`id_Region`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manager`
--

LOCK TABLES `manager` WRITE;
/*!40000 ALTER TABLE `manager` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `manager` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `manager_group`
--

DROP TABLE IF EXISTS `manager_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_group` (
  `id_Manager_group` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(70) NOT NULL,
  `Description` text,
  PRIMARY KEY (`id_Manager_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manager_group`
--

LOCK TABLES `manager_group` WRITE;
/*!40000 ALTER TABLE `manager_group` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `manager_group` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `manager_group_manager`
--

DROP TABLE IF EXISTS `manager_group_manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_group_manager` (
  `id_Manager` int(11) NOT NULL,
  `id_Manager_group` int(11) NOT NULL,
  PRIMARY KEY (`id_Manager`,`id_Manager_group`),
  KEY `ref_group_manager` (`id_Manager`),
  KEY `ref_manager_group` (`id_Manager_group`),
  CONSTRAINT `ref_group_manager` FOREIGN KEY (`id_Manager`) REFERENCES `manager` (`id_Manager`),
  CONSTRAINT `ref_manager_group` FOREIGN KEY (`id_Manager_group`) REFERENCES `manager_group` (`id_Manager_group`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manager_group_manager`
--

LOCK TABLES `manager_group_manager` WRITE;
/*!40000 ALTER TABLE `manager_group_manager` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `manager_group_manager` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `mock_efrsb_debtor`
--

DROP TABLE IF EXISTS `mock_efrsb_debtor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mock_efrsb_debtor` (
  `id_Debtor` int(11) NOT NULL AUTO_INCREMENT,
  `BankruptId` bigint(20) NOT NULL,
  `ArbitrManagerID` int(11) DEFAULT NULL,
  `id_Region` int(11) NOT NULL,
  `Body` longblob NOT NULL,
  `LastMessageDate` datetime DEFAULT NULL,
  `LastReportDate` datetime DEFAULT NULL,
  `INN` varchar(12) DEFAULT NULL,
  `SNILS` varchar(11) DEFAULT NULL,
  `Name` varchar(152) DEFAULT NULL,
  `OGRN` varchar(15) DEFAULT NULL,
  `DateLastModif` datetime NOT NULL,
  `Category` varchar(100) NOT NULL,
  `CategoryCode` varchar(50) NOT NULL,
  `Revision` bigint(20) NOT NULL,
  PRIMARY KEY (`id_Debtor`),
  UNIQUE KEY `byBankruptId_1` (`BankruptId`),
  KEY `refMockEfrsbManager_Debtor` (`id_Region`),
  KEY `byArbitrManagerID` (`ArbitrManagerID`),
  CONSTRAINT `refMockEfrsbManager_Debtor` FOREIGN KEY (`id_Region`) REFERENCES `mock_efrsb_region` (`id_Region`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mock_efrsb_debtor`
--

LOCK TABLES `mock_efrsb_debtor` WRITE;
/*!40000 ALTER TABLE `mock_efrsb_debtor` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `mock_efrsb_debtor` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `mock_efrsb_debtor_manager`
--

DROP TABLE IF EXISTS `mock_efrsb_debtor_manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mock_efrsb_debtor_manager` (
  `id_Debtor_Manager` int(11) NOT NULL AUTO_INCREMENT,
  `ArbitrManagerID` int(11) NOT NULL,
  `BankruptId` bigint(20) NOT NULL,
  `DateTime_MessageFirst` datetime NOT NULL,
  `DateTime_MessageLast` datetime NOT NULL,
  `Revision` bigint(20) NOT NULL,
  PRIMARY KEY (`id_Debtor_Manager`),
  KEY `debtor_manager_byArbitrManagerID` (`ArbitrManagerID`),
  KEY `byBankruptId` (`BankruptId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mock_efrsb_debtor_manager`
--

LOCK TABLES `mock_efrsb_debtor_manager` WRITE;
/*!40000 ALTER TABLE `mock_efrsb_debtor_manager` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `mock_efrsb_debtor_manager` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `mock_efrsb_manager`
--

DROP TABLE IF EXISTS `mock_efrsb_manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mock_efrsb_manager` (
  `id_Manager` int(11) NOT NULL AUTO_INCREMENT,
  `ArbitrManagerID` int(11) NOT NULL,
  `SRORegNum` varchar(30) DEFAULT NULL,
  `id_Region` int(11) DEFAULT NULL,
  `FirstName` varchar(50) NOT NULL,
  `MiddleName` varchar(50) DEFAULT NULL,
  `LastName` varchar(50) NOT NULL,
  `OGRNIP` varchar(15) DEFAULT NULL,
  `INN` varchar(12) NOT NULL,
  `DateLastModif` datetime NOT NULL,
  `DateReg` datetime DEFAULT NULL,
  `SRORegDate` datetime DEFAULT NULL,
  `DateDelete` datetime DEFAULT NULL,
  `Body` longblob NOT NULL,
  `RegNum` varchar(30) DEFAULT NULL,
  `DownloadDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Revision` bigint(20) NOT NULL,
  `CorrespondenceAddress` varchar(300) DEFAULT NULL,
  `SNILS` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`id_Manager`),
  UNIQUE KEY `byArbitrManagerID_1` (`ArbitrManagerID`),
  KEY `refMockEfrsbManager_Region` (`id_Region`),
  KEY `refMockEfrsbManager_SRO` (`SRORegNum`),
  CONSTRAINT `refMockEfrsbManager_Region` FOREIGN KEY (`id_Region`) REFERENCES `mock_efrsb_region` (`id_Region`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mock_efrsb_manager`
--

LOCK TABLES `mock_efrsb_manager` WRITE;
/*!40000 ALTER TABLE `mock_efrsb_manager` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `mock_efrsb_manager` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `mock_efrsb_region`
--

DROP TABLE IF EXISTS `mock_efrsb_region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mock_efrsb_region` (
  `id_Region` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id_Region`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mock_efrsb_region`
--

LOCK TABLES `mock_efrsb_region` WRITE;
/*!40000 ALTER TABLE `mock_efrsb_region` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `mock_efrsb_region` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `mock_efrsb_sro`
--

DROP TABLE IF EXISTS `mock_efrsb_sro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mock_efrsb_sro` (
  `id_SRO` int(11) NOT NULL,
  `OGRN` varchar(15) DEFAULT NULL,
  `RegNum` varchar(30) NOT NULL,
  `Body` longblob NOT NULL,
  `INN` varchar(10) NOT NULL,
  `DateLastModif` datetime NOT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `ShortTitle` varchar(250) DEFAULT NULL,
  `Title` varchar(250) DEFAULT NULL,
  `UrAdress` varchar(250) DEFAULT NULL,
  `Revision` bigint(20) NOT NULL,
  PRIMARY KEY (`id_SRO`),
  UNIQUE KEY `byRegNum` (`RegNum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mock_efrsb_sro`
--

LOCK TABLES `mock_efrsb_sro` WRITE;
/*!40000 ALTER TABLE `mock_efrsb_sro` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `mock_efrsb_sro` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `poll`
--

DROP TABLE IF EXISTS `poll`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poll` (
  `id_Poll` int(11) NOT NULL AUTO_INCREMENT,
  `id_SRO` int(11) DEFAULT NULL,
  `id_Region` int(11) DEFAULT NULL,
  `id_Manager_group` int(11) DEFAULT NULL,
  `Name` varchar(200) DEFAULT NULL,
  `ExtraColumns` longblob,
  `DateFinish` datetime DEFAULT NULL,
  `PollType` char(1) DEFAULT NULL,
  `Status` char(1) DEFAULT NULL,
  `TimeChange` datetime DEFAULT NULL,
  `TimeNotified` datetime DEFAULT NULL,
  PRIMARY KEY (`id_Poll`),
  KEY `refPoll_SRO` (`id_SRO`),
  KEY `refPoll_Region` (`id_Region`),
  KEY `ref_poll_manager_group` (`id_Manager_group`),
  CONSTRAINT `refPoll_Region` FOREIGN KEY (`id_Region`) REFERENCES `region` (`id_Region`),
  CONSTRAINT `refPoll_SRO` FOREIGN KEY (`id_SRO`) REFERENCES `sro` (`id_SRO`),
  CONSTRAINT `ref_poll_manager_group` FOREIGN KEY (`id_Manager_group`) REFERENCES `manager_group` (`id_Manager_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `poll`
--

LOCK TABLES `poll` WRITE;
/*!40000 ALTER TABLE `poll` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `poll` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `poll_document`
--

DROP TABLE IF EXISTS `poll_document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poll_document` (
  `id_Poll_document` int(11) NOT NULL AUTO_INCREMENT,
  `id_Poll` int(11) NOT NULL,
  `Poll_document_type` char(1) NOT NULL,
  `Poll_document_time` datetime NOT NULL,
  `FileName` varchar(70) NOT NULL,
  `Body` longblob NOT NULL,
  `Parameters` text NOT NULL,
  PRIMARY KEY (`id_Poll_document`),
  KEY `Ref_Poll_Document` (`id_Poll`),
  CONSTRAINT `Ref_Poll_Document` FOREIGN KEY (`id_Poll`) REFERENCES `poll` (`id_Poll`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `poll_document`
--

LOCK TABLES `poll_document` WRITE;
/*!40000 ALTER TABLE `poll_document` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `poll_document` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question` (
  `id_Question` int(11) NOT NULL AUTO_INCREMENT,
  `id_Poll` int(11) NOT NULL,
  `Title` varchar(250) NOT NULL,
  `Extra` longblob,
  PRIMARY KEY (`id_Question`),
  KEY `Ref_Question_Poll` (`id_Poll`),
  CONSTRAINT `Ref_Question_Poll` FOREIGN KEY (`id_Poll`) REFERENCES `poll` (`id_Poll`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `region`
--

DROP TABLE IF EXISTS `region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `region` (
  `id_Region` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(250) NOT NULL,
  PRIMARY KEY (`id_Region`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `region`
--

LOCK TABLES `region` WRITE;
/*!40000 ALTER TABLE `region` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `region` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `sro`
--

DROP TABLE IF EXISTS `sro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sro` (
  `id_SRO` int(11) NOT NULL AUTO_INCREMENT,
  `OGRN` varchar(15) DEFAULT NULL,
  `RegNum` varchar(30) NOT NULL,
  `INN` varchar(10) NOT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `ShortTitle` varchar(250) DEFAULT NULL,
  `Title` varchar(250) DEFAULT NULL,
  `UrAdress` varchar(250) DEFAULT NULL,
  `Revision` bigint(20) NOT NULL,
  PRIMARY KEY (`id_SRO`),
  UNIQUE KEY `byRegNum` (`RegNum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sro`
--

LOCK TABLES `sro` WRITE;
/*!40000 ALTER TABLE `sro` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `sro` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `tbl_migration`
--

DROP TABLE IF EXISTS `tbl_migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_migration` (
  `MigrationNumber` varchar(250) NOT NULL,
  `MigrationName` varchar(250) NOT NULL,
  `MigrationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`MigrationNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_migration`
--

LOCK TABLES `tbl_migration` WRITE;
/*!40000 ALTER TABLE `tbl_migration` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `tbl_migration` VALUES ('m001','AddPollRegionSro','2020-01-07 14:00:00'),('m002','DropUnncesessaryMockEfrsbTables','2020-01-07 14:00:00'),('m003','FixEfrsbMockTables','2020-01-07 14:00:00'),('m004','FixDebtorLegalCaseList','2020-01-07 14:00:00'),('m005','AddIndexes','2020-01-07 14:00:00'),('m006','AddVodingDocumentsLog','2020-01-07 14:00:00'),('m007','AddManagerPoll','2020-01-07 14:00:00'),('m008','AddQuestion','2020-01-07 14:00:00'),('m009','AddPollState','2020-01-07 14:00:00'),('m010','AddLogin','2020-01-07 14:00:00'),('m011','AddTimeNotified','2020-01-07 14:00:00'),('m012','AddClubAgreement','2020-01-07 14:00:00'),('m013','AddClubAgreementSignature','2020-01-07 14:00:00');
/*!40000 ALTER TABLE `tbl_migration` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `vote`
--

DROP TABLE IF EXISTS `vote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vote` (
  `id_Vote` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Manager` int(11) DEFAULT NULL,
  `id_Question` int(11) NOT NULL,
  `BulletinText` text,
  `BulletinVoterSignature` longblob,
  `BulletinOrpauSignature` longblob,
  `VoteData` text,
  `VoteTime` datetime DEFAULT NULL,
  `Confirmation_code` varchar(10) DEFAULT NULL,
  `Confirmation_code_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id_Vote`),
  KEY `Ref_Vote_Manager` (`id_Manager`),
  KEY `Ref_Vote_Question` (`id_Question`),
  CONSTRAINT `Ref_Vote_Manager` FOREIGN KEY (`id_Manager`) REFERENCES `manager` (`id_Manager`),
  CONSTRAINT `Ref_Vote_Question` FOREIGN KEY (`id_Question`) REFERENCES `question` (`id_Question`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vote`
--

LOCK TABLES `vote` WRITE;
/*!40000 ALTER TABLE `vote` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `vote` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `vote_document`
--

DROP TABLE IF EXISTS `vote_document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vote_document` (
  `id_Vote_document` int(11) NOT NULL AUTO_INCREMENT,
  `id_Vote` bigint(20) NOT NULL,
  `Vote_document_type` char(1) NOT NULL,
  `Vote_document_time` datetime NOT NULL,
  `FileName` varchar(80) NOT NULL,
  `Body` longblob NOT NULL,
  `Parameters` text NOT NULL,
  PRIMARY KEY (`id_Vote_document`),
  KEY `Ref_Vote_Document` (`id_Vote`),
  CONSTRAINT `Ref_Vote_Document` FOREIGN KEY (`id_Vote`) REFERENCES `vote` (`id_Vote`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vote_document`
--

LOCK TABLES `vote_document` WRITE;
/*!40000 ALTER TABLE `vote_document` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `vote_document` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `vote_log`
--

DROP TABLE IF EXISTS `vote_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vote_log` (
  `id_Vote_log` int(11) NOT NULL AUTO_INCREMENT,
  `id_Vote` bigint(20) NOT NULL,
  `Vote_log_type` char(1) NOT NULL,
  `Vote_log_time` datetime NOT NULL,
  `body` text,
  PRIMARY KEY (`id_Vote_log`),
  KEY `Ref_Vote_Log` (`id_Vote`),
  CONSTRAINT `Ref_Vote_Log` FOREIGN KEY (`id_Vote`) REFERENCES `vote` (`id_Vote`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vote_log`
--

LOCK TABLES `vote_log` WRITE;
/*!40000 ALTER TABLE `vote_log` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `vote_log` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping routines for database 'orpaudevel'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed
