
alter table poll add TimeChange datetime DEFAULT NULL;
alter table poll add TimeNotified datetime DEFAULT NULL;

insert into tbl_migration set MigrationNumber='m011', MigrationName='AddTimeNotified';
