
alter table debtor drop Text;
alter table debtor drop FOREIGN KEY `Ref_Debtor_Manager`;
alter table debtor drop KEY `Ref_Debtor_Manager`;
alter table debtor drop id_Manager;
alter table debtor drop FOREIGN KEY Refregion80;
alter table debtor drop KEY Ref5580;

alter table debtor change `LegalCaseList` `LegalCaseList` longblob;

alter table debtor add KEY `Ref_Debtor_Manager` (`ArbitrManagerID`);
alter table debtor add KEY `refDebtor_Region` (`id_Region`);
alter table debtor add CONSTRAINT `refDebtor_Region` FOREIGN KEY (`id_Region`) REFERENCES `region` (`id_Region`);

insert into tbl_migration set MigrationNumber='m004', MigrationName='FixDebtorLegalCaseList';
