
alter table debtor add KEY `byINN` (`INN`);
alter table debtor add KEY `byName` (`Name`);
alter table debtor add KEY `byOGRN` (`OGRN`);
alter table debtor add KEY `byRevision` (`Revision`);
alter table debtor add KEY `bySNILS` (`SNILS`);

alter table manager add KEY `byFirstName` (`FirstName`);
alter table manager add KEY `byINN` (`INN`);
alter table manager add KEY `byLastName` (`LastName`);
alter table manager add KEY `byMiddleName` (`MiddleName`);
alter table manager add KEY `byRevision` (`Revision`);

insert into tbl_migration set MigrationNumber='m005', MigrationName='AddIndexes';
