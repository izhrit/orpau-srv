alter table manager add ClubAgreementSignature longblob DEFAULT NULL;

insert into tbl_migration set MigrationNumber='m013', MigrationName='AddClubAgreementSignature';