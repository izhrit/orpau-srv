
alter table poll drop DateStart;
alter table poll add Status char(1) DEFAULT NULL;

insert into tbl_migration set MigrationNumber='m009', MigrationName='AddPollState';
