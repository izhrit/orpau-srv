CREATE TABLE `question` (
  `Extra` longblob,
  `Title` varchar(250) NOT NULL,
  `id_Poll` int(11) NOT NULL,
  `id_Question` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Question`),
  KEY `Ref_Question_Poll` (`id_Poll`),
  CONSTRAINT `Ref_Question_Poll` FOREIGN KEY (`id_Poll`) REFERENCES `poll` (`id_Poll`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table vote drop FOREIGN KEY Ref_Vote_Poll;
alter table vote drop KEY Ref_Vote_Poll;
alter table vote drop id_Poll;

alter table vote add id_Question int(11) NOT NULL;
alter table vote add KEY `Ref_Vote_Question` (`id_Question`);
alter table vote add CONSTRAINT `Ref_Vote_Question` FOREIGN KEY (`id_Question`) REFERENCES `question` (`id_Question`) ON DELETE CASCADE;

alter table vote_document drop FOREIGN KEY Ref_Vote_Document;
alter table vote_log drop FOREIGN KEY Ref_Vote_Log;

alter table vote change id_Vote id_Vote bigint(20) NOT NULL AUTO_INCREMENT;

alter table vote_document add CONSTRAINT `Ref_Vote_Document` FOREIGN KEY (`id_Vote`) REFERENCES `vote` (`id_Vote`) ON DELETE CASCADE;
alter table vote_log add CONSTRAINT `Ref_Vote_Log` FOREIGN KEY (`id_Vote`) REFERENCES `vote` (`id_Vote`) ON DELETE CASCADE;

alter table vote add Confirmation_code varchar(10) DEFAULT NULL;
alter table vote add Confirmation_code_time datetime DEFAULT NULL;

alter table manager add Salt varchar(50) DEFAULT NULL;

insert into tbl_migration set MigrationNumber='m008', MigrationName='AddQuestion';
