<?php

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/post.php';
require_once '../assets/helpers/time.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/realip.php';
require_once '../assets/helpers/sync_encrypt_decrypt.php';
require_once '../assets/actions/backend/manager/alib_manager.php';

require_once '../assets/libs/cryptoapi.php';

$dn_names_to_fix = array(
	'OID.1.2.643.3.131.1.1'=>'ИНН'
	, 'INN'=>'ИНН'
	, 'OID.1.2.840.113549.1.9.2'=>'UN'
);

function mb_trim($string, $trim_chars = '\s')
{
	return preg_replace('/^['.$trim_chars.']*(?U)(.*)['.$trim_chars.']*$/u', '\\1',$string);
}

function GetDnFields($text)
{
	$res= array();
	$parts= explode(',',$text);
	foreach ($parts as $part)
	{
		$pos= mb_strpos($part,'=');
		$name= mb_trim(mb_substr($part,0,$pos));

		global $dn_names_to_fix;
		if (isset($dn_names_to_fix[$name]))
			$name= $dn_names_to_fix[$name];

		$value= mb_substr($part,$pos+1);
		$res[$name]= $value;
	}
	return (object)$res;
}

function prepare_запрос_доступа($cert, $orpau_manager_fields)
{
	$current_time= safe_date_create();
	$data = (object)array(
		'время'=>date_format($current_time,'Y-m-d\TH:i:s')
		,'роль'=>'АУ'
		,'АУ'=>(object)array(
			'Фамилия'=>$orpau_manager_fields->LastName
			,'Имя'=>$orpau_manager_fields->FirstName
			,'Отчество'=>$orpau_manager_fields->MiddleName
			,'ИНН'=>$orpau_manager_fields->INN
			,'Рег_номер_ЕФРСБ'=>$orpau_manager_fields->RegNum
			,'EMail'=>$orpau_manager_fields->EMail
			,'Телефон'=>$orpau_manager_fields->Phone
			,'Login'=>$orpau_manager_fields->Login
			,'Регион'=>$orpau_manager_fields->Region
		)
		,'IP'=>getRealIPAddr()
	);
	if($cert)
	{
		$data->сертификат = (object)array(
			'IssuerName'=>$cert['Issuer']
			,'SerialNumber'=>$cert['SerialNumber']
		);
	}
	return $data;
}

function prepare_orpau_token_fields($запрос_доступа_fields, $orpau_manager_fields)
{
	$отметка_сайта= null;
	global $auth_token_options;
	if (!isset($_GET['open']) || (!isset($auth_token_options->enable_open) || !$auth_token_options->enable_open))
	{
		$token_fields_to_encrypt= nice_json_encode($запрос_доступа_fields);
		write_to_log_named('token_fields_to_encrypt',$token_fields_to_encrypt);
		$encrypted_token_fields= sync_encrypt($token_fields_to_encrypt,$auth_token_options);
		$отметка_сайта= md5($encrypted_token_fields);
	}
	return (object)array(
		'id_Manager'=>$orpau_manager_fields->id_Manager
		,'ArbitrManagerID'=>$orpau_manager_fields->ArbitrManagerID
		,'Отметка_сайта'=>$отметка_сайта
	);
}

function get_responce_body($curl_response)
{
	$pos= strpos($curl_response,"\r\n\r\n");
	$header= substr($curl_response,0,$pos);
	$curl_response= substr($curl_response,$pos+4);
	if (strpos($header,' 100 Continue'))
		$curl_response= get_responce_body($curl_response);
	return $curl_response;
}

function prepare_datamart_token_fields($запрос_доступа, $datamart_url)
{
	write_to_log('prepare_datamart_token_fields { ');
	$отметка_сайта= null;

	$url= "{$datamart_url}api.php?action=AuthByCert&cmd=get-token-to-sign";

	write_to_log("using $url");

	$curl = curl_init($url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HEADER, 1);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
	curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($запрос_доступа));
	$curl_response= curl_exec($curl);
	$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
	curl_close($curl);

	if (200==$httpcode)
	{
		$res= json_decode(get_responce_body($curl_response));
		write_to_log_named('prepare_datamart_token_fields }',$res);
		return $res;
	}
	else
	{
		write_to_log("got http code $httpcode from url $url");
		write_to_log('prepare_datamart_token_fields } return null');
		return null;
	}
}

function datamart_AuthByTokenSignature($на_витрину, $base64_encoded_signature, $datamart_url)
{
	write_to_log('datamart_AuthByTokenSignature { ');
	$url= "{$datamart_url}api.php?action=AuthByCert&cmd=auth-by-token-signature";

	write_to_log("using $url");

	$curl = curl_init($url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HEADER, 1);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
	curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode(array('на_витрину'=>$на_витрину,'base64_encoded_signature'=>$base64_encoded_signature)));
	$curl_response= curl_exec($curl);
	$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
	curl_close($curl);

	if (200==$httpcode)
	{
		$res= get_responce_body($curl_response);
		write_to_log_named('datamart_AuthByTokenSignature } return',$res);
		return json_decode($res);
	}
	else
	{
		write_to_log("got http code $httpcode from url $url");
		write_to_log('datamart_AuthByTokenSignature } return null ');
		return null;
	}
}


function prepare_token($token_fields)
{
	ob_start();
	include 'auth_request.html';
	$result = ob_get_contents();
	ob_end_clean();

	return $result;
}

function GetTokenToSignForManager($cert,$orpau_manager_fields)
{
	session_start();

	write_to_log('found manager:');
	write_to_log($orpau_manager_fields);

	$запрос_доступа_fields= prepare_запрос_доступа($cert, $orpau_manager_fields);

	global $base_apps_url;
	$token_without_datamart_fields= (object)array(
		'запрос_доступа'=>$запрос_доступа_fields
		,'на_сайт'=>array(
			$base_apps_url=>prepare_orpau_token_fields($запрос_доступа_fields, $orpau_manager_fields)
		)
	);

	$token_without_datamart= prepare_token($token_without_datamart_fields);
	$to_session= (object)array('without_datamart'=>(object)array('token_fields'=>$token_without_datamart_fields,'token'=>$token_without_datamart));
	$to_return= array('token'=>array('without_datamart'=>$token_without_datamart));

	global $ama_datamart_settings;
	$datamart_url= $ama_datamart_settings->use_base_url;
	$datamart_token_fields= prepare_datamart_token_fields($запрос_доступа_fields, $datamart_url);
	if (null!=$datamart_token_fields)
	{
		$token_with_datamart_fields= clone $token_without_datamart_fields;
		$token_with_datamart_fields->на_сайт[$datamart_url]= $datamart_token_fields;
		$token_with_datamart= prepare_token($token_with_datamart_fields);
		$to_session->with_datamart= (object)array('token_fields'=>$token_with_datamart_fields,'token'=>$token_with_datamart);
		$to_return['token']['with_datamart']= $token_with_datamart;
	}

	$_SESSION['auth_token_info']= $to_session;

	write_to_log('returns:');
	write_to_log($to_return);

	echo nice_json_encode($to_return);
}

function GetTokenToSign($cert)
{
	write_to_log('cert:');
	write_to_log($cert);

	$subject= GetDnFields($cert['Subject']);
	if (!isset($subject->ИНН))
		exit_bad_request("skipped ИНН in Subject!",$cert);

	write_to_log('ИНН:'.$subject->ИНН);

	$rows= execute_query(
		'select 
			m.id_Manager, m.ArbitrManagerID
			,m.LastName, m.FirstName, m.MiddleName
			,m.INN, m.RegNum, m.Phone, m.EMail, m.Login, r.Name Region
		from manager m
		left join region r on m.id_Region = r.id_Region
		where INN=?',
		array('s',$subject->ИНН));
	$count_rows= count($rows);

	if (1!=$count_rows)
	{
		exit_not_found("found $count_rows managers with INN={$subject->ИНН}");
	}
	else
	{
		GetTokenToSignForManager($cert,$rows[0]);
	}
}

function CheckTokenSignature($token,$base64_encoded_signature)
{
	global $base_cryptoapi_url, $base_cryptoapi_auth;

	$cryptoapi= new CryptoAPI((object)array(
		'base_url'=>$base_cryptoapi_url
		,'auth'=>$base_cryptoapi_auth
	));
	$result= $cryptoapi->VerifySignatureFromCapicom((object)array(
		'document'=>$token 
		,'base64_encoded_signature'=>$base64_encoded_signature
	));
	write_to_log_named('CheckTokenSignature result',$result);
	return $result->signature_checked;
}

function OkLogin($ainfo)
{
	global $_SESSION, $auth_info;
	if(!isset($_SESSION))
		session_start();

	$auth_token_info= $_SESSION['auth_token_info'];
	$token_fields= isset($auth_token_info->with_datamart)
		? $auth_token_info->with_datamart->token_fields
		: $auth_token_info->without_datamart->token_fields;

	$ainfo->logging= (object)array(
		'time'=> safe_date_create()
		,'IP'=> getRealIPAddr()
		,'cert'=> $token_fields->запрос_доступа->сертификат
	);

	$auth_info= $ainfo;
	$_SESSION['auth_info']= $ainfo;

	write_to_log_named('return',$ainfo);
	echo nice_json_encode($ainfo);
}

function AuthByTokenSignature($data_signature)
{
	write_to_log('data_signature:');
	write_to_log($data_signature);

	session_start();
	if (!isset($_SESSION['auth_token_info']))
	{
		exit_bad_request("undefined session auth_token_info");
	}
	else
	{
		$token_variant= $data_signature['variant'];
		$token_info= $_SESSION['auth_token_info'];
		//write_to_log_named('session auth_token_info',$token_info);
		$token_info= $token_info->$token_variant;
		$token= $token_info->token;
		$base64_encoded_signature= $data_signature['base64_encoded_signature'];
		if (!CheckTokenSignature($token,$base64_encoded_signature))
		{
			exit_unauthorized("can not check signature!");
		}
		else
		{
			write_to_log('signature is checked!');
			$token_fields= $token_info->token_fields;
			$АУ= $token_fields->запрос_доступа->АУ;

			global $base_apps_url;
			$orpau_params= $token_fields->на_сайт[$base_apps_url];
			$agreementsState = GetAgreementsState($orpau_params->id_Manager);

			$auth_info = (object)array(
				'id_Manager'=> $orpau_params->id_Manager
				,'ArbitrManagerID'=> $orpau_params->ArbitrManagerID
				,'Фамилия'=> $АУ->Фамилия, 'Имя'=> $АУ->Имя, 'Отчество'=> $АУ->Отчество
				,'ИНН'=> $АУ->ИНН, 'efrsbNumber'=> $АУ->Рег_номер_ЕФРСБ
				,'ИспользуетАСП'=>$agreementsState->AspAgreementState
				,'СостоитВПрофсоюзе'=>$agreementsState->ClubAgreementState
				,'Login'=>$АУ->Login
			);

			write_to_log("token_variant=$token_variant");
			if ('with_datamart'==$token_variant)
			{
				global $ama_datamart_settings;
				$datamart_url= $ama_datamart_settings->use_base_url;
				$auth_info->datamart= datamart_AuthByTokenSignature($token_fields->на_сайт[$datamart_url],$base64_encoded_signature,$datamart_url);
			}

			OkLogin($auth_info);
		}
	}
}

function Logout()
{
	session_start();
	unset($_SESSION['token_info']);
	unset($_SESSION['auth_info']);
	echo nice_json_encode((object)array('ok'=>true));
	write_to_log('ok');
}

function GetTokenByInn($post_data)
{
	write_to_log('post_data:');
	write_to_log($post_data);

	$inn = $post_data['inn'];
	$rows= execute_query(
		'select 
			m.id_Manager, m.ArbitrManagerID
			,m.LastName, m.FirstName, m.MiddleName
			,m.INN, m.RegNum, m.Phone, m.EMail, m.Login, r.Name Region
		from manager m
		left join region r on m.id_Region = r.id_Region
		where INN=?',
		array('s',$inn));
	$count_rows= count($rows);

	if (1!=$count_rows)
	{
		exit_not_found("found $count_rows managers with INN={$subject->ИНН}");
	}
	else
	{
		GetTokenToSignForManager(null, $rows[0]);
	}
}

CheckMandatoryGET('cmd');

switch ($_GET['cmd'])
{
	case 'get-token-to-sign':
		write_to_log('cmd:get-token-to-sign');
		$data_to_auth= safe_POST($_POST);
		CheckMandatoryPOST('Subject',$data_to_auth);
		CheckMandatoryPOST('Issuer',$data_to_auth);
		CheckMandatoryPOST('SerialNumber',$data_to_auth);
		GetTokenToSign($data_to_auth);
		break;
	case 'auth-by-token-signature':
		write_to_log('cmd:auth-by-token-signature');
		$data_signature= safe_POST($_POST);
		CheckMandatoryPOST('base64_encoded_signature',$data_signature);
		AuthByTokenSignature($data_signature);
		break;
	case 'get-token-by-inn':
		write_to_log('cmd:get-token-by-inn');
		$data_signature= safe_POST($_POST);
		CheckMandatoryPOST('inn',$data_signature);
		GetTokenByInn($data_signature);
		break;
	case 'logout':
		write_to_log('cmd:logout');
		Logout();
		break;
}