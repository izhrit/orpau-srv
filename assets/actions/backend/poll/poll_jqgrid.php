<?php 

require_once '../assets/config.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';

$fields= "
	id_Poll
	,poll.Name
	,DateFinish
	,Status
	,if(sro.id_SRO is not null,concat('СРО ',if(sro.ShortTitle is null,sro.Name,sro.ShortTitle)),
	 if(region.id_Region is not null,concat('регион ',region.Name),
	 if(manager_group.id_Manager_group is not null,concat('группа ',manager_group.Name),
	 'все'))) Who
";
$from_where= "from poll 
left join sro on sro.id_SRO=poll.id_SRO
left join region on region.id_Region=poll.id_Region
left join manager_group on manager_group.id_Manager_group=poll.id_Manager_group
WHERE id_Poll=id_Poll ";

$filter_rule_builders= array(
	'Name'=> function($l,$rule)
	{	
		$data= mysqli_real_escape_string($l,$rule->data);
		return " and poll.Name like '%$data%' ";
	}
);
execute_query_for_jqgrid($fields,$from_where,$filter_rule_builders,' order by DateFinish desc, id_Poll desc');