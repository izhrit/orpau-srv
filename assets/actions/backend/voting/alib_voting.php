<?php

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/post.php';
require_once '../assets/helpers/time.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/alib_email_dispatch_to_db.php';

function PrepareLog($id_Vote,$descr,$body=null)
{
	global $Vote_log_type_description;
	$d= FindDescription($Vote_log_type_description,'descr',$descr);
	$code= $d['code'];
	$log_time= safe_date_create();
	$log_time_sql= date_format($log_time,'Y-m-d\TH:i:s');
	$log_time_json= date_format($log_time,'d.m.Y H:i:s');
	$txt_query= "insert into vote_log set id_Vote=?, Vote_log_type=?, Vote_log_time=?, body=?;";
	execute_query_get_last_insert_id($txt_query,array('ssss',$id_Vote,$code,$log_time_sql,$body));
	$res= array('Vote_log_type'=>$code,'Vote_log_time'=>$log_time_json);
	if (null!=$body)
		$res['body']= $body;
	return $res;
}

function PrepareDocument($id_Vote,$log_time,$code,$body)
{
	global $operator_pem_file_path, $Vote_document_type_description;

	$log_time_sql= date_format($log_time,'Y-m-d\TH:i:s');
	$log_time_json= date_format($log_time,'d.m.Y H:i:s');

	$d= FindDescription($Vote_document_type_description,'code',$code);

	$extension= ($d['Наименование_документа']=='Подпись Единой ИС Арбитражных управляющих' && null==$operator_pem_file_path) ? 'txt' : $d['FileName_extension'];

	$filename= $d['FileName_prefix'].'.'.date_format($log_time,'YmdHis').'.'.$extension;
	$txt_query= "insert into vote_document set id_Vote=?, Vote_document_type=?, Vote_document_time=?, FileName=?, Body=compress(?), Parameters=?;";
	$id_Vote_document= execute_query_get_last_insert_id($txt_query,array('ssssss',$id_Vote,$code,$log_time_sql,$filename,$body, $d['Наименование_документа']));

	return (object)array('id_Vote_document'=>$id_Vote_document,'Vote_document_time'=>$log_time_json,
		'Vote_document_type'=>$code,'FileName'=>$filename, 'Размер'=>strlen($body), 'doc_md5'=>md5($body), 'Body'=>$body);
}

function fix_end_of_lines($str)
{
	return str_replace("\n","\r\n",str_replace("\r\n","\n",$str));
}

function PostLetter($letter, $row, $EmailType_descr)
{
	mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
	$connection= default_dbconnect();
	$connection->begin_transaction();
	try
	{
		$result = Dispatch_Email_Message($connection, $letter,$row->EMail,$row->FullName,
			$EmailType_descr,$row->id_Manager);
		if (false==$result)
		{
			$connection->rollback();
			exit_internal_server_error("Can not send email to $row->EMail");
		}
		else
		{
			$connection->commit();
		}
	}
	catch (mysqli_sql_exception $ex)
	{
		$connection->rollback();
	}
	catch (Exception $ex)
	{
		$connection->rollback();
		throw $ex;
	}
}

function Направить_email_участнику_Голосования($id_Vote, $row, $тип_события, $дополнительно= null, $writeLog = true)
{
	global $Параметры_для_писем_по_голосованию;
	if (!isset($Параметры_для_писем_по_голосованию[$тип_события]))
		throw new Exception("unknown voting letter parameters for event_type=$тип_события");
	$p= $Параметры_для_писем_по_голосованию[$тип_события];

	$параметры= !isset($дополнительно['параметры']) ? null : $дополнительно['параметры'];

	$letter= (object)array('subject'=>"{$p->Начало_темы_письма} {$row->PollName}");
	ob_start();
	include "../assets/actions/backend/voting/views/letters/{$p->Шаблон_письма}.php";
	$letter->body_txt= fix_end_of_lines(ob_get_contents());
	ob_end_clean();

	if (null!=$дополнительно && isset($дополнительно['вложения']))
	{
		$attachments= array();
		foreach ($дополнительно['вложения'] as $row_Vote_document)
		{
			$attachments[]= array(
				'filename'=>$row_Vote_document->FileName
				, 'content'=>$row_Vote_document->Body
			);
		}
		$letter->attachments= $attachments;
	}

	try {
		PostLetter($letter, $row, $p->Тип_письма);
	} catch (Exception $ex) {
		write_to_log("cant post letter problem params: $letter, $row, $p->Тип_письма");
		throw $ex;
	}
	if ($writeLog)
		PrepareLog($id_Vote, $тип_события, $row->EMail);
}

function FindDescription($description_array,$key_name,$key_value)
{
	foreach ($description_array as $d)
	{
		if ($key_value==$d[$key_name])
			return $d;
	}
	return null;
}

function isValidEmail($email) {
	return filter_var($email, FILTER_VALIDATE_EMAIL)
		&& preg_match('/@.+\./', $email);
}

function send_voting_email_by_vote($id_Vote, $событие, $дополнительно = null)
{
	$txt_query = "select
			m.id_Manager
			, m.EMail
			, concat(m.LastName,' ',m.FirstName,' ',m.MiddleName) FullName
			, p.Name PollName
			, uncompress(p.ExtraColumns) ExtraPoll
			, p.DateFinish
			, uncompress(v.VoteData) ExtraVote
			, v.VoteTime
			, v.Confirmation_code
			, v.Confirmation_code_time
		from manager m
		inner join vote v on v.id_Manager=m.id_Manager
		left join question q on v.id_Question=q.id_Question
		left join poll p on p.id_Poll=q.id_Poll
		where v.id_Vote=? and m.EMail is not null";
	$manager_rows= execute_query($txt_query, array('s', $id_Vote));
	$manager_row = $manager_rows[0];

	if (!isValidEmail($manager_row->EMail))
	{
		exit_internal_server_error("Not valid email to $manager_row->EMail");
	}
	else
	{
		$decode_poll= json_decode($manager_row->ExtraPoll);
		$manager_row->ExtraPoll=$decode_poll;
		$decode_vote= json_decode($manager_row->ExtraVote);
		$manager_row->ExtraVote=$decode_vote;

		Направить_email_участнику_Голосования($id_Vote, $manager_row, $событие, $дополнительно);
	}
}

function cmp_documents($a, $b)
{
	$ta= $a->time;
	$tb= $b->time;
	return ($ta < $tb) ? -1 : 1;
}

function build_FileName_for_SentEmail($row)
{
	global $email_Template_specs;
	if (!isset($row->TimeSent) || null==$row->TimeSent)
	{
		return '';
	}
	else
	{
		$d= FindDescription($email_Template_specs,'code',$row->EmailType);
		$filename= (null==$d || !isset($d['FileName_prefix'])) ? 'Unknown' : $d['FileName_prefix'];
		$filename.= '.';
		$TimeSent= date_create_from_format('Y-m-d H:i:s',$row->TimeDispatch);
		$filename.= date_format($TimeSent,'YmdHis');
		$filename.= '.eml';
		return $filename;
	}
}

function Load_ЖурналДокументы(&$vote_cabinet)
{
	$txt_query_select_Vote_document= "select 
			Vote_document_time
			, Vote_document_type
			, FileName
			, id_Vote_document
			, if(Body is null,null,md5(uncompress(Body))) doc_md5
			, if(Body is null,null,length(uncompress(Body))) `Размер`
		from vote_document 
		where id_Vote= ?";

	$txt_query= "select 
       vl.Vote_log_type
     , vl.Vote_log_time
     , vl.body 
	 , p.id_Poll
	from vote_log vl
	left join vote v on vl.id_Vote = v.id_Vote
	left join question q on q.id_Question=v.id_Question
	left join poll p on q.id_Poll = p.id_Poll
	where vl.id_Vote= ? and 
	    (vl.Vote_log_type!='n' && vl.Vote_log_type!='r' &&
	    vl.Vote_log_type!='o' && vl.Vote_log_type!='u' || 
	    vl.Vote_log_type='u')";
	$log_rows= execute_query($txt_query,array('i',$vote_cabinet['id_Vote']));

	$id_Poll=$log_rows[0]->id_Poll;
	$txt_query="select
	    Vote_log_type,
	    Vote_log_time,
	    body,
	    p.id_Poll
	from vote_log vl
		left join vote v on vl.id_Vote = v.id_Vote
		left join question q on q.id_Question=v.id_Question
		left join poll p on q.id_Poll = p.id_Poll
	where p.id_Poll=? and 
	    (vl.Vote_log_type='n' || vl.Vote_log_type='r' ||
	    vl.Vote_log_type='o' || vl.Vote_log_type='t' || 
	    vl.Vote_log_type='u')";
	$other_log_rows=execute_query($txt_query, array('s', $id_Poll));

	$log_rows_merged = array_merge($log_rows, $other_log_rows);
	foreach ($log_rows_merged as $row)
	{
		$st= $row->Vote_log_time;
		$t= date_create_from_format('Y-m-d H:i:s',$st);
		$st= date_format($t,'d.m.Y H:i:s');
		$row->Vote_log_time= $st;
	}
	write_to_log($log_rows_merged);
	$vote_cabinet['Журнал']= $log_rows_merged;

	$rows_Vote_document= execute_query($txt_query_select_Vote_document,array('i',$vote_cabinet['id_Vote']));
//	$rows_SentEmail= execute_query($txt_query_select_SentEmail,array('i',$vote_cabinet['id_Vote']));
//	$documents= array_merge($rows_Vote_document, $rows_SentEmail);
	$documents = $rows_Vote_document;

	foreach ($documents as $doc)
	{
		if (!isset($doc->FileName))
			$doc->FileName= build_FileName_for_SentEmail($doc);
		$st= isset($doc->Vote_document_time) ? $doc->Vote_document_time : $doc->TimeDispatch;
		$t= date_create_from_format('Y-m-d H:i:s',$st);
		$doc->time= $t;
		$st= date_format($t,'d.m.Y H:i:s');
		if (isset($doc->Vote_document_time))
		{
			$doc->Vote_document_time= $st;
		}
		else
		{
			$doc->TimeDispatch= $st;
			if (null!=$doc->ExtraParams)
				$doc->ExtraParams= json_decode($doc->ExtraParams);
		}
	}
	usort($documents, "cmp_documents");
	foreach ($documents as $doc)
		unset($doc->time);

	$vote_cabinet['Документы']= $documents;
}

function smime_p7s_to_sig($smime_p7s)
{
	$prefix= 'Content-Disposition: attachment; filename="smime.p7s"';
	$pos1= strpos($smime_p7s,$prefix) + strlen($prefix);
	$pos2= strpos($smime_p7s,'------',$pos1);
	$sig= substr($smime_p7s,$pos1,$pos2-$pos1);
	return trim($sig);
}

function prepareSignatureOfOperator($txt)
{
	global $operator_pem_file_path;

	if (null==$operator_pem_file_path)
	{
		return 'Заменитель подписи в тестовом режиме (настройка пути к сертификату НЕ задана)';
	}
	else
	{
		$temp_file = tmpfile();
		try
		{
			$meta_data= stream_get_meta_data($temp_file);
			$temp_file_path = $meta_data['uri'];
			$temp_file_path_sig = $temp_file_path.'.sig';
			fwrite($temp_file, $txt);
			fflush($temp_file);

			try
			{
				openssl_pkcs7_sign($temp_file_path, $temp_file_path_sig, $operator_pem_file_path,
					array($operator_pem_file_path, ""),
					array(),PKCS7_DETACHED | PKCS7_BINARY
				);
				$smime_p7s= file_get_contents($temp_file_path_sig);
				unlink($temp_file_path_sig);
			}
			catch (Exception $ex)
			{
				unlink($temp_file_path_sig);
				throw $ex;
			}
			fclose($temp_file);
		}
		catch (Exception $ex)
		{
			fclose($temp_file);
			throw $ex;
		}
		$signature= smime_p7s_to_sig($smime_p7s);
		return $signature;
	}
}

$Vote_document_type_description= array(
	array('code'=>'b', 'descr'=>'b', 'Наименование_документа'=>'Бюллетень', 'FileName_prefix'=>'B', 'FileName_extension'=>'txt')
	,array('code'=>'s', 'descr'=>'s', 'Наименование_документа'=>'АСП бюллетеня', 'FileName_prefix'=>'S', 'FileName_extension'=>'txt')
	,array('code'=>'r', 'descr'=>'r', 'Наименование_документа'=>'Подпись ИС АСП', 'FileName_prefix'=>'O', 'FileName_extension'=>'sig')
	,array('code'=>'k', 'descr'=>'k', 'Наименование_документа'=>'Подпись бюллетеня', 'FileName_prefix'=>'P', 'FileName_extension'=>'sig')
);

$Vote_log_type_description= array(
	array('code'=>'n', 'descr'=>'Голосование', 'text'=>'Уведомление о голосовании на адрес')
	,array('code'=>'s', 'descr'=>'Подписан бюллетень', 'text'=>'Подпись бюллетеня на адрес')
	,array('code'=>'a', 'descr'=>'Выбран ответ', 'text'=>'Выбран ответ для вопроса')
	,array('code'=>'c', 'descr'=>'Отправлен код', 'text'=>'Отправлен код подтверждения на номер')
	,array('code'=>'b', 'descr'=>'Письмо о подписи', 'text'=>'Отправление бюллетеня на адрес')
	,array('code'=>'r', 'descr'=>'Итоги голосования', 'text'=>'Уведомление об итогах голосования на адрес') //Итоги заседания
	,array('code'=>'o', 'descr'=>'Отменено', 'text'=>'Уведомление об отмене на адрес')
	,array('code'=>'w', 'descr'=>'Неверный код', 'text'=>'Введён неверный код подтверждения')
	,array('code'=>'d', 'descr'=>'Код прислан поздно', 'text'=>'Слишком поздно введён код подтверждения')
	,array('code'=>'t', 'descr'=>'Обновлено', 'text'=>'Уведомление о обновлении голосования на адрес')
	,array('code'=>'е', 'descr'=>'Приостановлено', 'text'=>'Уведомление о приостановке голосования на адрес')
	,array('code'=>'u', 'descr'=>'Продолжено', 'text'=>'Уведомление о продолжении голосования на адрес')

//todo ниже шаблоны которые не используются
	,array('code'=>'l', 'descr'=>'Вход в кабинет', 'text'=>'Осуществлён вход в кабинет с IP ')
	,array('code'=>'f', 'descr'=>'Не состоялось', 'text'=>'Уведомление, заседание НЕ состоялось, на адрес')
	,array('code'=>'z', 'descr'=>'Продолжено без вас', 'text'=>'Уведомление о блокировании кабинета на адрес')
);

$Параметры_для_писем_по_голосованию= array
(
	'Голосование'=> (object)array(
		'Начало_темы_письма' => 'Новое голосование'
		, 'Шаблон_письма' => 'b_starts'
		, 'Тип_письма'=>'о новом голосовании'
	)
	,'Обновлено'=> (object)array(
		'Начало_темы_письма' => 'Обновлено голосование'
		, 'Шаблон_письма' => 'b_updated'
		, 'Тип_письма'=>'голосование обновлено'
	)
	,'Приостановлено'=> (object)array(
		'Начало_темы_письма' => 'Приостановлено голосование'
		, 'Шаблон_письма' => 'c_paused'
		, 'Тип_письма'=>'голосование приостановлено'
	)
	,'Продолжено'=> (object)array(
		'Начало_темы_письма' => 'Возобновлено голосование'
		, 'Шаблон_письма' => 'b_starts_again'
		, 'Тип_письма'=>'голосование продолжено'
	)
	,'Отменено'=> (object)array(
		'Начало_темы_письма' => 'Отменено голосование'
		, 'Шаблон_письма' => 'g_stop'
		, 'Тип_письма'=>'голосование отменено'
	)
	,'Итоги голосования'=> (object)array(
		'Начало_темы_письма' => 'Результаты голосования'
		, 'Шаблон_письма' => 'e_took_place_fix_results'
		, 'Тип_письма'=>'об итогах голосования'
	)
	,'Письмо о подписи'=> (object)array(
		'Начало_темы_письма' => 'Уведомление о подписании бюллетеня голосования'
		, 'Шаблон_письма' => 'bc_get_code'
		, 'Тип_письма'=>'о начале подписания'
	)
	,'Подписан бюллетень'=> (object)array(
		'Начало_темы_письма' => 'Подтверждение подписания бюллетеня голосования'
		, 'Шаблон_письма' => 'bc_got_code'
		, 'Тип_письма'=>'о подписании'
	)
//todo ниже шаблоны которые не используются
	,'Не состоялось'=> (object)array(
		'Начало_темы_письма' => 'НЕ состоялось заседание'
		, 'Шаблон_письма' => 'f_did_not_take_place'
		, 'Тип_письма'=>'КК не состоялось'
	)
	,'Продолжено без вас'=> (object)array(
		'Начало_темы_письма' => 'Без вас возобновлено заседание'
		, 'Шаблон_письма' => 'b_starts_again_without'
		, 'Тип_письма'=>'КК продолжено без вас'
	)
);

$poll_status_description=array(
	array('code'=>'n', 'descr'=>'Создано')
	,array('code'=>'v', 'descr'=>'Голосование')
	,array('code'=>'p', 'descr'=>'Приостановлено')
	,array('code'=>'c', 'descr'=>'Отменено')
	,array('code'=>'e', 'descr'=>'Завершено')
	,array('code'=>'u', 'descr'=>'Продолжено')
);