<?php 

global $auth_info, $base_apps_url, $email_settings;

?>Аналог собственноручной подписи АУ бюллетеня голосования

<?php echo  date_format($auth_info->logging->time,'d.m.Y H:i:s') ?><?php if (0) { ?>10.01.2021 10:00<?php } ?> 
Пользователь №<?php echo  $auth_info->id_Manager ?><?php if (0) { ?>234<?php } ?> "<?php echo  
	$vote->FullName ?><?php if (0) { ?>Иванов Иван Иванович<?php } ?>", 
(далее именуемый Пользователь) с IP <?php echo  $auth_info->logging->IP ?><?php if (0) { ?>123.456.789.012<?php } ?> 
вошёл в систему <?php echo  $base_apps_url ?><?php if (0) { ?>http://rosau.ru<?php } ?> 
<?php if (isset($auth_info->logging->login)) { ?>
по логину <?php echo  $auth_info->logging->login ?><?php if (0) { ?>hz<?php } ?> и паролю.
<?php } ?>
<?php if (isset($auth_info->logging->cert)) { ?>
по сертификату с полями:
	IssuerName: <?php echo  $auth_info->logging->cert->IssuerName ?><?php if (0) { ?>CN:HZ<?php } ?> 
	SerialNumber: <?php echo  $auth_info->logging->cert->SerialNumber ?><?php if (0) { ?>345<?php } ?>
<?php } ?> 

<?php echo  $запись_об_уведомление_о_голосовании->Vote_log_time ?> 
Пользователь приступил к подписанию.

Сформированный для подписания документ 
"Бюллетень голосования <?php echo  $vote->QuestionTitle ?>"
был направлен Пользователю с адреса <?php echo  $email_settings->smtp_from_email ?><?php if (0) { ?>no-reply@rosau.ru<?php } ?> 
на электронный адрес <?php echo  $vote->EMail ?><?php if (0) { ?>aaa@oo.ru<?php } ?> 
в электронном письме с темой 
«Уведомление о подписании бюллетеня голосования <?php echo  $vote->QuestionTitle ?>»
приложенным файлом "<?php echo  $документ_бюллетень->FileName ?><?php if (0) { ?>ОРПАУ.txt<?php } ?>", 
размером <?php echo  $документ_бюллетень->Размер ?><?php if (0) { ?>123989<?php } 
?> байт и контрольной суммой 
md5 <?php echo  $документ_бюллетень->doc_md5 ?><?php if (0) { ?>578949384579385987<?php } ?> 

Одноразовый ключ АСП <?php echo  $vote->Confirmation_code ?><?php if (0) { ?>678<?php } ?> был направлен пользователю
при помощи СМС на номер <?php echo  $запись_об_отправке_кода->body ?><?php if (0) { ?>349057394578<?php } ?> 

<?php echo  $запись_об_отправке_кода->Vote_log_time ?> 
Пользователь ввёл одноразовый ключ АСП <?php echo  $vote->Confirmation_code ?><?php if (0) { ?>678<?php } ?> 
в специальное интерактивное окно ввода сайта под визуальным образом 
подписываемого документа, для создания аналога собственноручной подписи.

Данный текст — Аналог собственноручной подписи АУ заверяется электронной 
подписью компании РИТ 
