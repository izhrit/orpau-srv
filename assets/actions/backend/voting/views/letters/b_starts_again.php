Уважаемый(ая) <?php echo  $row->FullName ?>,

Уведомляем Вас об ПРОДОЛЖЕНИИ голосования
«<?php echo  $row->PollName ?>»,
итоги которого должны подводиться <?php echo  $row->DateFinish ?>.

Повестка дня голосования:
<?php $number= 1; ?>
<?php foreach ($row->ExtraPoll->Вопросы as $вопрос) : ?>
<?php echo  $number ?>) <?php echo  $вопрос->В_повестке ?>

<?php $number++ ?>
<?php endforeach; ?>

Бюллетени голосования принимаются от участников в Кабинете АУ на странице сайта Единой ИС Арбитражных уравляющих.
Дата и время окончания приема бюллетеней голосования: <?php echo 
$row->DateFinish ?> по местному времени.

C Уважением, Администрация Единой ИС Арбитражных уравляющих.