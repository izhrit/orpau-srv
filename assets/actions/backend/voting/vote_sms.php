<?php

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/post.php';
require_once '../assets/helpers/time.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/actions/backend/voting/alib_voting.php';
require_once '../assets/helpers/SMSTransport.php';
require_once '../assets/helpers/password.php';

require_once '../assets/libs/auth/check.php';

$auth_info= CheckAuth();

function safeSendSmsConfirmationCode($Confirmation_code,$vote)
{
	global $sms_settings, $auth_info;
	if (false==$sms_settings->enabled)
	{
		return false;
	}
	else
	{
		$txt= "{$Confirmation_code} – код подтверждения подписи Единой ИС Арбитражных управляющих для голосования по вопросу «{$vote->QuestionTitle}>»";
		$sms = new SMSTransport($sms_settings);
		write_to_log_named('vote',$vote);
		write_to_log("send sms from {$sms_settings->FROM} to {$vote->Phone}: $txt");
		$res= $sms->send($vote->Phone,$txt,$sms_settings->FROM);
		write_to_log($res);
		return $res;
	}
}

function Read_vote($id_vote)
{
	$txt_query = "select
			v.id_Vote
			,v.Confirmation_code
			,v.Confirmation_code_time
			,v.VoteData 
			,m.id_Manager
			,m.FirstName
			,m.LastName
			,m.MiddleName
			,concat(m.LastName,' ',m.FirstName,' ',m.MiddleName) FullName
			,m.Phone
			,m.EMail
			,q.Title QuestionTitle
		from vote v
		left join manager m on m.id_Manager=v.id_Manager
		left join question q on v.id_Question = q.id_Question
		left join poll p on q.id_Poll = p.id_Poll
		where v.id_Vote=?
	";

	$rows= execute_query($txt_query,array('s', $id_vote));

	$question=$rows[0];
	$question->ФИО=$question->LastName.' '.$question->FirstName.' '.$question->MiddleName;
	return $question;
}

function Выслать_проверочный_код()
{
	global $sms_settings, $email_settings;

	$row= Read_vote($_GET['id_Vote']);

	$post_data= safe_POST($_POST);
	$bulletin_text = $post_data['text'];
	$row_Vote_doc_bulletin= PrepareDocument($row->id_Vote,safe_date_create(),'b',$bulletin_text);

	$дополнительно = array(
		'вложения'=>array($row_Vote_doc_bulletin)
		,'параметры'=>(object)array(
			'бюллетень'=> (object)array(
				'md5'=>$row_Vote_doc_bulletin->doc_md5
				,'длина'=>$row_Vote_doc_bulletin->Размер
				,'FileName'=>$row_Vote_doc_bulletin->FileName
			)
		)
	);
	send_voting_email_by_vote($_GET['id_Vote'], 'Письмо о подписи', $дополнительно);

	$log_time= safe_date_create();
	$log_time_json= date_format($log_time,'d.m.Y H:i:s');

	$Confirmation_code=  (true!=$email_settings->enabled)
		? '1234' : generate_password(4,array('0','1','2','3','4','5','6','7','8','9'));

	safeSendSmsConfirmationCode($Confirmation_code,$row);

	$txt_query= "update vote set Confirmation_code=?, Confirmation_code_time=?, BulletinText=? where id_Vote=?";
	execute_query_no_result($txt_query
		,array('ssss',$Confirmation_code,date_format($log_time,'Y-m-d\TH:i:s'),$bulletin_text,$row->id_Vote));

	PrepareLog($row->id_Vote,'Отправлен код',$row->Phone);

	$res= array(
		'sms'=>array('number'=>$row->Phone, 'time'=>$log_time_json)
		,'email'=>array('address'=>$row->EMail, 'time'=>$log_time_json/*$row_SentEmail_doc->TimeDispatch*/)
	);
	if (false==$sms_settings->enabled)
		$res['test_code']= $Confirmation_code;
	echo nice_json_encode($res);
	exit;
}

function Catch_Code()
{
	$row= Read_vote($_GET['id_Vote']);
	$posted_data= safe_POST($_POST);
	$received_code= $posted_data['code'];

	$current_time= safe_date_create();
	$Confirmation_code_time= date_create_from_format('Y-m-d H:i:s',$row->Confirmation_code_time);
	$diff= date_diff($current_time,$Confirmation_code_time);

	$max_minutes_to_confirm_sms_code= 30;
	if (total_minutes($diff)>$max_minutes_to_confirm_sms_code)
	{
		PrepareLog($row->id_Vote,'Код прислан поздно',"\"$received_code\"");
		Clear_Confirmation_code($row);
		echo nice_json_encode(array('ok'=>false,'why'=>"Код подтверждения предъявлен слишком поздно (спустя более $max_minutes_to_confirm_sms_code минут )!"));
	}
	else if ($received_code!=$row->Confirmation_code)
	{
		PrepareLog($row->id_Vote,'Неверный код',"\"$received_code\" вместо высланного \"{$row->Confirmation_code}\"");
		Clear_Confirmation_code($row);
		echo nice_json_encode(array('ok'=>false,'why'=>'Указанный код подтверждения не соответствует высланному!'));
	}
	else
	{
		Catch_code_ok($row->id_Vote,$row,$posted_data);
	}
}

function Clear_Confirmation_code($row)
{
	$txt_query= "update vote set Confirmation_code=null, Confirmation_code_time=null where id_Vote=?";
	execute_query_no_result($txt_query,array('s',$row->id_Vote));
}

function total_minutes($date_diff)
{
	$minutes = $date_diff->days * 24 * 60;
	$minutes += $date_diff->h * 60;
	$minutes += $date_diff->i;
	return $minutes;
}

function Catch_code_ok($id_Vote,$vote,$data)
{
	$ЖурналДокументы= array('id_Vote'=>$id_Vote);
	Load_ЖурналДокументы($ЖурналДокументы);
	$vote->Журнал= $ЖурналДокументы['Журнал'];
	$vote->Документы= $ЖурналДокументы['Документы'];

	$log_time= safe_date_create();

	write_to_log($vote);
	$txt_signature= prepareSignature($vote, $log_time);
	$row_Vote_document_sig= PrepareDocument($id_Vote,$log_time,'s',$txt_signature);

	$operator_signature= prepareSignatureOfOperator($txt_signature);
	$row_Vote_document_rit_sig= PrepareDocument($id_Vote,safe_date_create(),'r',$operator_signature);

	$дополнительно = array(
		'вложения'=>array($row_Vote_document_sig,$row_Vote_document_rit_sig)
		,'параметры'=>(object)array(
			'row_Vote_document_sig'=> $row_Vote_document_sig
			,'row_Vote_document_rit_sig'=> $row_Vote_document_rit_sig
		)
	);
	send_voting_email_by_vote($_GET['id_Vote'], 'Подписан бюллетень', $дополнительно);

	$log_time_json= date_format(safe_date_create(),'d.m.Y H:i:s');

	$vote_data= json_decode($vote->VoteData);
	$vote_data->Подписано=$log_time_json;

	execute_query_no_result(
		"update vote set 
                VoteData=?
              , BulletinVoterSignature=?
              , Confirmation_code=null
              , Confirmation_code_time=null 
		where id_Vote=?;"
		,array('sss',nice_json_encode($vote_data), $txt_signature, $id_Vote)
	);

	$res= array('ok'=>true,'Подписано'=>$log_time_json);
	echo nice_json_encode($res);
	exit;
}

function prepareSignature($vote, $log_time)
{
	global $sms_settings, $email_settings;
	write_to_log('prepareSignature {');
	$log_time_json= date_format($log_time,'d.m.Y H:i:s');
	$подписано_сертификатом = false;
	write_to_log('prepareSignature 0');
	$запись_об_уведомление_о_голосовании= findVote_log($vote, 'b');
	write_to_log($запись_об_уведомление_о_голосовании);
	write_to_log('prepareSignature 1');
	if (empty($запись_об_уведомление_о_заседании))
		$запись_об_уведомление_о_заседании= findVote_log($vote, 'u');
	write_to_log('prepareSignature 2');
	$запись_о_гололосовании=findVote_log($vote, 'b');
	write_to_log('prepareSignature 3');
	$документ_бюллетень= findVote_document($vote,'b');
	write_to_log('prepareSignature 4');
	$запись_об_отправке_кода= findVote_log($vote,'c');
	write_to_log('prepareSignature 5');
	$вход_с_устройства = $_SERVER['REMOTE_ADDR'];

	ob_start();
	include "../assets/actions/backend/voting/views/documents/signature.php";
	$signature= fix_end_of_lines(ob_get_contents());
	ob_end_clean();

	return $signature;
}

function findSentEmail($vote,$EmailType)
{
	$count= count($vote->Документы);
	for ($i= $count-1; $i>=0; $i--)
	{
		$doc= $vote->Документы[$i];
		if (isset($doc->EmailType) && $doc->EmailType==$EmailType)
			return $doc;
	}
	return null;
}

function findVote_document($vote,$Vote_document_type)
{
	$count= count($vote->Документы);
	for ($i= $count-1; $i>=0; $i--)
	{
		$doc= $vote->Документы[$i];
		if (isset($doc->Vote_document_type) && $doc->Vote_document_type==$Vote_document_type)
			return $doc;
	}
	return null;
}

function findVote_log($vote,$Vote_log_type)
{
	global $Vote_log_type_description, $Параметры_для_писем_по_голосованию;
	$count= count($vote->Журнал);
	for ($i= $count-1; $i>=0; $i--)
	{
		$log= $vote->Журнал[$i];
		if ($log->Vote_log_type==$Vote_log_type)
		{
			if ($Vote_log_type=='n' || $Vote_log_type=='u')
			{
				$d= FindDescription($Vote_log_type_description,'code',$Vote_log_type);
				$p= $Параметры_для_писем_по_голосованию[$d['descr']];
				$log->Subject=$p->Начало_темы_письма;
			}
			write_to_log_named('Vote_log_type',$Vote_log_type);
			write_to_log_named('Vote_log_time',$log->Vote_log_time);
			/*$date_format = date_create_from_format('d.m.Y H:i:s',$log->Vote_log_time);
			if (false!=$date_format)
				$log->Vote_log_time = date_format($date_format, 'd.m.Y H:i');*/
			return $log;
		}
	}
	return null;
}

$cmd= $_GET['cmd'];
switch ($cmd)
{
	case 'throw-code':
		Выслать_проверочный_код();
		break;
	case 'catch-code':
		Catch_code();
		break;
	default:
		exit_bad_request("unknown cmd=$cmd");
}