<?php

require_once '../assets/config.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/db_fill_row.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/libs/auth/check.php';

$auth_info= CheckAuth();

class Fill_manager_group_row_query_builder extends Fill_row_query_builder
{
	public function comma_separated_params($group)
	{
		$this->comma_par('Name', $group->Name);
		$this->comma_par('Description', $group->Description);
	}
}
class Fill_manager_group_manager_row_query_builder extends Fill_row_query_builder
{
	public function comma_separated_params($id_Manager, $id_Group)
	{
		$this->comma_par('id_Manager', $id_Manager, 'i');
		$this->comma_par('id_Manager_group', $id_Group, 'i');
	}
}

class Group_crud extends Base_crud
{
	function create($group)
	{
		if(trim($group['Name']) == null)
		{
			echo 'Введите название группы';
			exit_bad_request('Введите название группы');
		}
		$qb= new Fill_manager_group_row_query_builder('insert into manager_group set ');
		$qb->comma_separated_params((object)$group);
		$id_Group= $qb->execute_query_get_last_insert_id();
		foreach($group['Состав_АУ'] as $row)
		{
			$mqb=new Fill_manager_group_manager_row_query_builder('insert into manager_group_manager set ');
			$mqb->comma_separated_params($row['id'], $id_Group);
			$mqb->execute_query_no_result();
		}
		echo '{ "ok": true }';
	}

	function read($id_Group)
	{
		$txt_query= 'select 
			id_Manager_group
			,Name
			,Description
		from manager_group
		where id_Manager_group=?;';
		$rows= execute_query($txt_query,array('s',$id_Group));
		if (0==count($rows))
		{
			exit_not_found("can not find manager_group id_Manager_group=$id_Group");
		}
		else
		{
			$txt_query_managers = 'select
                m.id_Manager id
                ,m.FirstName firstName
                ,m.LastName lastName
                ,m.MiddleName middleName
                ,concat(m.LastName, " ", m.FirstName, " ", m.MiddleName) text
                ,m.INN inn
                ,sro.Name sro
                ,r.Name region
            from manager m
            left join sro on sro.RegNum=m.SRORegNum
            left join region r on r.id_Region=m.id_Region
            inner join manager_group_manager mg on mg.id_Manager=m.id_Manager
            where mg.id_Manager_group=?';
			$managersRows = execute_query($txt_query_managers, array('s', $id_Group));

			$row= $rows[0];
			$group= array(
				'id_Manager_group'=>$row->id_Manager_group
				,'Name'=>$row->Name
				,'Description'=>$row->Description
				,'Состав_АУ'=>$managersRows
			);
			echo nice_json_encode($group);
		}
	}

	function update($id_Group,$group)
	{
		$qb= new Fill_manager_group_row_query_builder('update manager_group set ');
		$qb->comma_separated_params((object)$group);
		$qb->parametrized("\r\nwhere id_Manager_group=?",$id_Group);
		$affected_rows= $qb->execute_query_get_affected_rows();

		$dqb= new Fill_row_query_builder('delete from manager_group_manager where');
		$dqb->parametrized(' id_Manager_group=?', $id_Group, 'i');
		$dqb->execute_query_no_result();

		foreach ($group['Состав_АУ'] as $row)
		{
			$mqb= new Fill_manager_group_manager_row_query_builder('insert into manager_group_manager set ');
			$mqb->comma_separated_params($row['id'], $id_Group);
			$mqb->execute_query_no_result();
		}

		if (1==$affected_rows)
		{
			echo '{ "ok": true }';
		}
		else if (0==$affected_rows)
		{
			echo '{ "ok": true, "affected": 0 }';
		}
		else
		{
			exit_bad_request("affected $affected_rows when update id_Manager_group=$id_Group");
		}
	}

	function delete($id_Groups)
	{
		mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
		$connection= default_dbconnect();
		$connection->begin_transaction();
		foreach ($id_Groups as $id_Group)
		{
			$qb= new Fill_row_query_builder('delete from manager_group where');
			$qb->parametrized(' id_Manager_group=?',$id_Group,'i');
			$affected= $qb->execute_query_get_affected_rows($connection);
			if (1!=$affected)
			{
				$connection->rollback();
				exit_not_found("can not delete manager_group where id_Manager_group=$id_Group (affected $affected rows)");
			}
		}
		$connection->commit();
		echo '{ "ok": true }';
	}
}

$crud = new Group_crud();
$crud->process_cmd();