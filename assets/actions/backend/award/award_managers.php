<?
require_once  __DIR__.'/../../../config.php';
require_once __DIR__.'/../../../helpers/log.php';
require_once __DIR__.'/../../../helpers/json.php';
require_once __DIR__.'/../../../helpers/validate.php';

$q= !isset($_GET['q']) ? '' : $_GET['q'];

$curl = curl_init("http://probili.ru/efrsb/GetManagersForSelect2.php?q=$q");
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HEADER, 1);

$curl_response= curl_exec($curl);
$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
curl_close($curl);

if (200!=$httpcode)
	exit_internal_server_error("can not request manager list");

$pos= strpos($curl_response,"\r\n\r\n");
echo substr($curl_response,$pos+4);

