<?
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';

$voter_inn = $_POST['VoterINN'];
$id_Nominee = $_POST['id_Nominee'];
$voter_cms = $_POST['cms'];
$bulletin = $_POST['bulletin'];
$vote_data = $_POST['vote_data'];
$poll = json_decode($_POST['poll']);

$orpau_cms = SignBulletinText($bulletin);

$txt_query = "SELECT id_Manager from manager WHERE inn = ?";

$id_Manager = execute_query($txt_query, array('s', $voter_inn))[0]->id_Manager;

$txt_query = 
"insert into vote 
(id_Manager, id_Poll, BulletinText, BulletinVoterSignature, BulletinOrpauSignature, id_Nominee, VoteTime, VoteData)
VALUES(?, ?, ?, ?, ?, ?, now(), ?)
on duplicate key update BulletinVoterSignature=?";

execute_query($txt_query, 
	array('ssssssss', $id_Manager, $poll->id_Poll, $bulletin, $voter_cms, $orpau_cms, $id_Nominee, $vote_data, $voter_cms));

function SignBulletinText($Text)
{
	global $crypto_api_url;
	$Curl = curl_init();
	curl_setopt_array($Curl, array(
		CURLOPT_URL => $crypto_api_url.'Sign',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_POST => true,
		CURLOPT_POSTFIELDS => $Text
	));
	$signature = curl_exec($Curl);
	$httpcode = curl_getinfo($Curl, CURLINFO_HTTP_CODE);
	if (200!=$httpcode)
		exit_internal_server_error("can not request sign code $httpcode for url \"$url\")");
	return $signature;
}