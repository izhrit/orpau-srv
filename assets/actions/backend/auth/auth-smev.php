<?php 

require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';

/*require_once '../assets/libs/auth/check.php';

$auth_info= CheckAuthAdmin();

if (!isset($auth_info->permissions['crmw']) || !$auth_info->permissions['crmw'])
	exit_unauthorized("no permission for crmw!");*/

session_start();


require_once '../assets/helpers/validate.php';
require_once '../assets/config/remote_api.php';

$api_settings= $remote_api_settings->smev;

$auth_info= $_SESSION['auth_info'];
$inn= $auth_info->ИНН;

$url= "{$api_settings->base_url}api-orpau.php?action=auth&token={$api_settings->token}&password={$api_settings->password}&manager-inn=$inn";
$curl = curl_init($url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_FAILONERROR, 1);
curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
$responce_txt= curl_exec($curl);
$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
curl_close($curl);

if (200!=$httpcode)
	exit_internal_server_error("got code $httpcode for url \"$url\")");

$responce= json_decode($responce_txt);

if (null==$responce)
	exit_internal_server_error("can not parse response from \"$url\": $responce_txt)");

if (!isset($responce->ok) || !$responce->ok)
	exit_unauthorized("got !ok from \"$url\": $responce_txt");

$res= array(
	'ok'=>true
	,'access_token'=>$responce->access_token
	,'stop_unix_time'=>$responce->stop_unix_time
	,'for_token'=>$responce->for_token
);

echo nice_json_encode($res);

