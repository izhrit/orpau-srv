<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/json.php';
require_once '../assets/config.php';
require_once '../assets/libs/auth/check.php';

$auth_info= CheckAuth();

function GetInfo($id_Manager) {
	$txt_query= "select
			m.id_Manager
			, m.FirstName
			, m.LastName
			, m.MiddleName
			, m.RegNum
			, m.INN
			, m.CorrespondenceAddress
			, m.Phone
			, m.Email
			, m.Password
     		, m.Login
			, m.ArbitrManagerID
			, sro.ShortTitle SROName
			, sro.RegNum SRORegNum
		from manager m
		left join sro on m.SRORegNum = sro.RegNum
		where m.id_Manager= ?";

	$rows= execute_query($txt_query, array('s',$id_Manager));
	$count_rows= count($rows);
	if(1!=$count_rows){
		exit_not_found("found $count_rows managers with id_Manager={$id_Manager}");
	} else {
		$row = $rows[0];
		echo nice_json_encode($row);
	}
}

function GetConfirmationCode($id_Manager) {
	$txt_query = "select
       		v.id_Vote
			, v.Confirmation_code Code
			, q.Title Question
		from vote v
		inner join question q on v.id_Question = q.id_Question
		where v.id_Manager=? and v.Confirmation_code is not null";

	$rows= execute_query($txt_query, array('s',$id_Manager));

	echo nice_json_encode($rows);
}

CheckMandatoryGET('cmd');
CheckMandatoryGET('id');

$cmd= $_GET['cmd'];
$id= $_GET['id'];
switch ($cmd)
{
	case 'get-info':
		GetInfo($id);
		break;
	case 'get-code':
		GetConfirmationCode($id);
		break;
	default:
		exit_bad_request("unknown cmd=$cmd");
}