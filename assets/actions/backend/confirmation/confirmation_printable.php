<?php

require_once '../assets/helpers/db.php';
require_once '../assets/config.php';

if (isset($_GET['id_Confirmation']))
{
	$id_Confirmation = $_GET['id_Confirmation'];
	$txt_query = "SELECT Text FROM confirmation WHERE id_Confirmation = ?";
	$res = execute_query($txt_query, array('s', $id_Confirmation));
	ob_start();
	include '../assets/actions/backend/templates/printable_confirmation_template.php';
	$result = ob_get_contents();
	ob_end_clean();
	echo $result;
}
else if (isset($_GET['id_Debtor'])&&isset($_GET['id_Manager']))
{
	$generated_flag = true;
	ob_start();
	require_once '../assets/actions/backend/confirmation/confirmation_generator.php';
	$res[0] = (object)array();
	$res[0]->Text = ob_get_contents();
	ob_end_clean();

	ob_start();
	include '../assets/actions/backend/templates/printable_confirmation_template.php';
	$result = ob_get_contents();
	ob_end_clean();

	echo $result;
}
else throw Exception('Должен быть установлен id_Confirmation, либо id_Manager и id_Debtor');