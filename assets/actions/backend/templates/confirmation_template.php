Справка о полномочиях Арбитражного управляющего № {Номер_справки}

Подготовлена <?php echo $today?> в отношении следующих лиц:

— <?php echo $manager->LastName?> <?php echo $manager->FirstName?> <?php echo $manager->MiddleName?>,
  далее — АУ, с реквизитами:
<?php if (isset($manager->INN)) {?>
	ИНН <?php echo $manager->INN?><?php } ?> <?php if (isset($manager->PostAdress)) {?>

	Адрес для корреспонденции: <?php echo $manager->PostAdress?><?php } ?> <?php if (isset($manager->EMail)) {?>

	Электронная почта <?php echo $manager->EMail?><?php } ?> <?php if (isset($manager->Phone)) {?>

	Телефон <?php echo $manager->Phone?><?php } ?> 

— <?php echo $debtor->Name?>,
  далее — Должник, с реквизитами:
<?php if (isset($debtor->INN)) {?>
	ИНН <?php echo $debtor->INN?><?php } if (isset($debtor->OGRN)) {?>

	ОГРН <?php echo $debtor->OGRN?><?php } if (isset($debtor->SNILS)) {?>

	СНИЛС <?php echo $debtor->SNILS?><?php } ?>	
	
Вышеозначенный АУ действует в рамках Федерального закона "О несостоятельности (банкротстве)" от 26.10.2002 N 127-ФЗ как арбитражный управляющий в процедуре над Должником (далее - Процедура)

<?php if (isset($debtor->LegalCaseList)){?>
Процедура ведётся в рамках судебного дела <?php
	$decoded_cases = json_decode($debtor->LegalCaseList);
	$cases = '';
	for($i=0; $i<count($decoded_cases); $i++)
	{
		$cases.=$decoded_cases[$i]->Number;
		if ($i<count($decoded_cases)-1)
		{
			$cases.=', ';
		}
	}	?><?php echo $cases.'.'?><?php } ?>


Общероссийский профсоюз арбитражных управляющих (далее - ОРПАУ) подтверждает полномочия АУ в Процедуре в соответствии с Законодательством РФ и пп. 11, 12, 13 ст. 4 устава ОРПАУ.

ОРПАУ выражает свою готовность защищать интересы АУ в рамках Процедуры в соответствии с законодательством.

ОРПАУ при необходимости готов сообщить или подтвердить контактные данные АУ и предоставить дополнительные справки о его полномочиях.

Справка подготовленна на основании следующих данных:

<?php if (isset($manager->SROName)) {?>
— АУ принят в СРО <?php echo $manager->SROName?> <?php if (isset($manager->SRORegDate)) {?> <?php echo $manager->SRORegDate?><?php } ?>, сведений о его исключении или дисквалификации не обнаружено.
<?php } ?>

<?php if (isset($messageNumber)) {?>
— На сайте единого федерального реестра сведений о банкротстве опубликовано сообщение №<?php echo $messageNumber?> в отношении должника за подписью АУ, сообщений от других АУ после этой даты не обнаружено.
<?php } ?>