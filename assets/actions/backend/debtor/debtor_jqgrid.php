<?php
require_once '../assets/config.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';

$fields= "
	Name as title
	,OGRN as ogrn 
	,INN as inn
	,SNILS as snils
	,id_Debtor
";
$from_where= "from debtor WHERE id_Debtor=id_Debtor ";
if (isset($_GET['ArbitrManagerID']))
{
	$ArbitrManagerID= $_GET['ArbitrManagerID'];
	if (!is_numeric($ArbitrManagerID))
		exit_bad_request("bad numeric _GET['ArbitrManagerID']");
	$from_where.= " and ArbitrManagerID=$ArbitrManagerID ";
}
$filter_rule_builders= array(
	'title'=> function($l,$rule)
	{
		$data= mysqli_real_escape_string($l,$rule->data);
		return " AND debtor.Name like '$data%'";
	}
	,'ogrn'=> function($l,$rule)
	{
		$data= mysqli_real_escape_string($l,$rule->data);
		return " AND debtor.OGRN like '$data%'";
	}
	,'inn'=> function($l,$rule)
	{
		$data= mysqli_real_escape_string($l,$rule->data);
		return " AND debtor.INN like '$data%'";
	}
	,'snils'=> function($l,$rule)
	{
		$data= mysqli_real_escape_string($l,$rule->data);
		return " AND debtor.SNILS like '$data%'";
	}
	,'id_Debtor'=>array('id_Debtor'=>'query_field')
);
execute_query_for_jqgrid($fields,$from_where,$filter_rule_builders);