<?php

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/config.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/validate.php';

function getRequestID()
{
	CheckPostForGetRequestID();
	if (CheckLicenseToken($_POST['licenseToken'], $_POST['ContractNumber'], $_POST['Manager']))
	{
		session_start();
		$_SESSION['RequestID'] = uniqid();
		$_SESSION['Procedure'] = $_POST['Procedure'];
		$_SESSION['Manager'] = $_POST['Manager'];
		$_SESSION['Token'] = $_POST['licenseToken'];
		echo nice_json_encode(array("RequestID"=>$_SESSION['RequestID']));
	}
	else
	{
		exit_bad_request('License token validation failed');
	}
}

function CheckPostForGetRequestID()
{
	if (!isset($_POST['licenseToken']))
		exit_bad_request('skipped $_POST["licenseToken"]');
	if (!isset($_POST['ContractNumber']))
		exit_bad_request('skipped $_POST["ContractNumber"]');
	if (!isset($_POST['Procedure']))
		exit_bad_request('skipped $_POST["Procedure"]');
	if (!isset($_POST['Manager']))
		exit_bad_request('skipped $_POST["Manager"]');
	if (!isset($_POST['Manager']['firstName']))
		exit_bad_request('skipped $_POST["Manager"]->firstName');
	if (!isset($_POST['Manager']['lastName']))
		exit_bad_request('skipped $_POST["Manager"]->lastName');
	if (!isset($_POST['Manager']['lastName']))
		exit_bad_request('skipped $_POST["Manager"]->middleName');
	if (!isset($_POST['Manager']['INN']))
		exit_bad_request('skipped $_POST["Manager"]->INN');
	return true;
}

function getConfirmationInfo()
{
	CheckSessionForGetConfirmationInfo();
	CheckPostForGetConfirmationInfo();
	session_start();
	if ($_SESSION['Token'] == $_POST['licenseToken'])
	{
		if ($_SESSION['RequestID'] == $_POST['RequestID'])
		{
			$txt_query = "SELECT id_Confirmation FROM confirmation
			JOIN manager ON id_Manager 
			WHERE firstName = ? AND lastName = ?
			AND middleName = ? AND inn = ?";
			$confirmationNumber = execute_query(
				$txt_query, 
				array(
					"ssss",
					$_SESSION['Manager']->firstName, $_SESSION['Manager']->lastName,
					$_SESSION['Manager']->middleName, $_SESSION['Manager']->INN,
				)
			)[0]->id_Confirmation;
			global $base_app_url;
			$confirmationRef = $base_app_url.'?action=confirmation.printable&cmd=get&id_Confirmation='.$confirmationNumber;
		}
		else exit_bad_request('bad RequestID');
	}
	else exit_bad_request('bad token');
}

function CheckSessionForGetConfirmationInfo()
{
	if (!isset($_SESSION['RequestID']))
		exit_bad_request('skipped $_SESSION["RequestID"]');
	if (!isset($_SESSION['Procedure']))
		exit_bad_request('skipped $_SESSION["Procedure"]');
	if (!isset($_SESSION['Token']))
		exit_bad_request('skipped $_SESSION["Token"]');
	if (!isset($_SESSION['Manager']))
		exit_bad_request('skipped $_SESSION["Manager"]');
	if (!isset($_SESSION['Manager']->firstName))
		exit_bad_request('skipped $_SESSION["Manager"]->firstName');
	if (!isset($_SESSION['Manager']->lastName))
		exit_bad_request('skipped $_SESSION["Manager"]->lastName');
	if (!isset($_SESSION['Manager']->lastName))
		exit_bad_request('skipped $_SESSION["Manager"]->middleName');
	if (!isset($_SESSION['Manager']->INN))
		exit_bad_request('skipped $_SESSION["Manager"]->INN');
	
}

function CheckPostForGetConfirmationInfo()
{
	if(!isset($_POST["licenseToken"]))
		exit_bad_request('skipped $_POST["licenseToken"]');
	if(!isset($_POST["RequestID"]))
		exit_bad_request('skipped $_POST["RequestID"]');
}

function CheckLicenseToken($license_token, $ContractNumber, $manager= null)
{
	global $use_server_license_url;
	$use_server_license_url = 'https://dlicense.rsit.ru/ama/';
	$url= $use_server_license_url 
		. "?action=validateTokenForDataMart"
		. "&license_token=".$license_token
		. "&contract=".$ContractNumber
		. "&ip=".'5.227.126.152';/*.getRealIPAddr();*/

	if (null==$manager)
	{
		$url.= '&user=customer';
	}
	else
	{
		$url.= "&user=manager"
			. "&lastName=".$manager['lastName']
			. "&firstName=".$manager['firstName']
			. "&middleName=".$manager['middleName'];
	}

	$response_txt = file_get_contents($url);
	$response=json_decode($response_txt);
	if (isset($response->status) && 'true'==$response->status)
	{
		return true;
	}
	else 
	{
		write_to_log("server license url:\r\n$url");
		write_to_log("returns:");
		write_to_log($response);
		return false;
	}
}

if(!isset($_GET['cmd']))
	exit_bad_request('skipped $GET_["cmd"]');
$cmd= $_GET['cmd'];
switch ($cmd)
{
	case 'getRequestID':
		getRequestID(); 
		break;
	case 'getConfirmationInfo':
		getConfirmationInfo('id');
		break;
	default:
		exit_bad_request("unknown cmd \"$cmd\"");
}