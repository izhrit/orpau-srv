<?php

require_once '../assets/libs/alib_email_dispatch_to_db.php';

function Prep_Письмо_уведомление_о_подписании($ау,$документ)
{
	ob_start();
	include '../assets/libs/asp/tpl/notify.html';
	$уведомление= ob_get_contents();
	ob_end_clean();
	return $уведомление;
}

function Отправить_уведомление_о_подписании_документа($connection,$ау,$подписываемый_документ,$tpl_descr)
{
	$title= $подписываемый_документ['title'];
	$letter= (object)array
	(
		'subject'=>"Уведомление о подписании документа \"$title\""
		,'body_txt'=> Prep_Письмо_уведомление_о_подписании($ау,$подписываемый_документ)
		,'attachments'=>array($подписываемый_документ)
	);
	$to_name= $ау->Фамилия.' '.$ау->Имя.' '.$ау->Отчество;
	return Dispatch_Email_Message($connection, $letter, $ау->EMail,
		$to_name, $tpl_descr, $ау->id_Manager);
}

