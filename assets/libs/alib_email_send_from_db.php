<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/time.php';

global $id_Email_Sender;
$id_Email_Sender= null;

function safe_get_id_Email_Sender($connection)
{
	global $id_Email_Sender;
	if (null!=$id_Email_Sender)
	{
		return $id_Email_Sender;
	}
	else
	{
		global $email_settings;
		$SenderServer= $email_settings->smtp_server;
		$SenderUser= $email_settings->smtp_user;
		trace("let's find SenderServer:\"$SenderServer\", SenderUser:\"$SenderUser\"");
		$args= array('ss',$SenderServer,$SenderUser);
		$rows= $connection->execute_query
			('select id_Email_Sender from email_sender where SenderServer=? and SenderUser=?;',$args);
		$rows_count= count($rows);
		if (1==$rows_count)
		{
			$id_Email_Sender= $rows[0]->id_Email_Sender;
			trace("found id_Email_Sender=$id_Email_Sender");
			return $id_Email_Sender;
		}
		else if (0!=$rows_count)
		{
			throw new Exception("too many email_sender rows for server:$SenderServer, user:$SenderUser");
		}
		else 
		{
			trace("can not find sender, let's create new!");
			$id_Email_Sender= $connection->execute_query_get_last_insert_id
				('insert into email_sender set SenderServer=?, SenderUser=?;',$args);
			trace("created id_Email_Sender=$id_Email_Sender");
			return $id_Email_Sender;
		}
	}
	
}

function Удалить_задание_на_отправку($connection,$row_email_to_send)
{
	trace("got id_Email_Message={$row_email_to_send->id_Email_Message} to send and delete it from email_tosend");
	$connection->execute_query(
		'delete from email_tosend where id_Email_Message=?;',
		array('s',$row_email_to_send->id_Email_Message));
}

function Записать_результат_удачной_отправки($connection,$row_email_to_send,$mime_message)
{
	trace('sent.');
	$connection->execute_query(
		'insert into email_sent set id_Email_Message=?, id_Email_Sender=?, TimeSent=?, Message=?;',
		array('ssss'
			,$row_email_to_send->id_Email_Message
			,safe_get_id_Email_Sender($connection)
			,date_format(safe_date_create(),'Y-m-d\TH:i:s')
			,json_encode($mime_message)));
}

function Записать_результат_неудачной_отправки($connection,$row_email_to_send,$error_text)
{
	trace("fail: $error_text");
	$connection->execute_query(
		'insert into email_error set id_Email_Message=?, id_Email_Sender=?, TimeError=?, Description=?;',
		array('ssss'
			,$row_email_to_send->id_Email_Message
			,safe_get_id_Email_Sender($connection)
			,date_format(safe_date_create(),'Y-m-d\TH:i:s')
			,$error_text));
}

function ExceptionErrorText($ex)
{
	$ex_class= get_class($ex);
	$ex_Message= $ex->getMessage();
	$error_text= "$ex_class - $ex_Message";
	return $error_text;
}

function SendEmailMessage($connection,$row_email_to_send)
{
	Удалить_задание_на_отправку($connection,$row_email_to_send);

	$rows_attachments= $connection->execute_query("
		select
			FileName, Content
		from email_attachment
		where id_Email_Message=?
		order by id_Email_Attachment;",
		array('s',$row_email_to_send->id_Email_Message));

	$mime_message= null;
	try
	{
		$mime_message= SendEmail($row_email_to_send, $rows_attachments);
	}
	catch (Exception $ex)
	{
		Записать_результат_неудачной_отправки($connection,$row_email_to_send,ExceptionErrorText($ex));
		return;
	}
	Записать_результат_удачной_отправки($connection,$row_email_to_send,$mime_message);
}

global $txt_query_select_to_send;
$txt_query_select_to_send= "select
	  e.id_Email_Message id_Email_Message
	, e.RecipientId RecipientId
	, e.RecipientType RecipientType
	, e.RecipientEmail RecipientEmail
	, e.EmailType EmailType
	, e.TimeDispatch TimeDispatch
	, uncompress(e.Details) Details
from email_tosend t
inner join email_message e on e.id_Email_Message=t.id_Email_Message
order by t.TimeDispatch desc
limit 10
;";

function SendPortionOfEmailMessages($max_portion_size= 100)
{
	global $txt_query_select_to_send;
	$rows_email_to_send_count= 0;
	$count_sent_email= 0;
	mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
	$connection= default_dbconnect();
	do
	{
		$rows_email_to_send= $connection->execute_query($txt_query_select_to_send,array());
		$rows_email_to_send_count= count($rows_email_to_send);
		if (0!=$rows_email_to_send_count)
		{
			if (0==$count_sent_email)
				trace("------------ send email portion job ---------------- {");
			trace("got $rows_email_to_send_count to send");
			foreach ($rows_email_to_send as $row_email_to_send)
			{
				$count_sent_email++;
				$row_email_to_send->ExtraParams= json_decode($row_email_to_send->Details);
				SendEmailMessage($connection,$row_email_to_send);
			}
		}
		
	}
	while ($rows_email_to_send_count>0 && $count_sent_email<$max_portion_size);
	if (0!=$count_sent_email)
		trace("------------ send email portion job ---------------- }");
}
