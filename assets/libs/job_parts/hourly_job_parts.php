<?php

require_once '../assets/libs/efrsb/sync_efrsb_sro.php';
require_once '../assets/libs/efrsb/sync_efrsb_manager.php';
require_once '../assets/libs/efrsb/sync_efrsb_debtor.php';
require_once '../assets/libs/efrsb/sync_efrsb_debtor_manager.php';

$hourly_job_parts= array
(
	 
);

global $job_params;

if (isset($job_params->sync_with_efrsb) && true==$job_params->sync_with_efrsb)
{
	$hourly_job_parts= array_merge($hourly_job_parts,array(
	 array('exec'=>'sync_sros',              'title'=>'sync orpau sro table with efrsb')
	,array('exec'=>'sync_managers',          'title'=>'sync orpau manager table with efrsb')
	,array('exec'=>'sync_debtors',           'title'=>'sync orpau debtor table with efrsb')
	,array('exec'=>'sync_debtor_managers',   'title'=>'sync orpau debtor_manager table with efrsb')
	));
}