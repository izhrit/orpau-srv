<?php

require_once '../assets/libs/poll_state/notify_status_change.php';

require_once '../assets/helpers/trace.php';
require_once '../assets/libs/alib_email_send_from_db.php';
require_once '../assets/libs/alib_email_send.php';

$minutely_job_parts= array
(
	 array('exec'=>'check_poll_change_date', 'title'=>'dispatch notification on change poll state')
);

global $job_params;

if (isset($job_params->send_emails) && true==$job_params->send_emails)
{
	$minutely_job_parts= array_merge($minutely_job_parts,array(
	 array('exec'=>'SendPortionOfEmailMessages', 'title'=>'send emails')
	));
}

