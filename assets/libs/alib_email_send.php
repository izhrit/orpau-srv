<?php

require_once '../assets/PHPMailer/PHPMailerAutoload.php';

function SendEmail($row_email_to_send, $rows_attachment)
{
	global $email_settings;

	$mail = new PHPMailer;
	$mail->CharSet = 'UTF-8';
	$mail->isSMTP();                                      // Set mailer to use SMTP
	$mail->Host = $email_settings->smtp_server;           // Specify main and backup SMTP servers
	if (isset($email_settings->smtp_user) && null!=$email_settings->smtp_user)
	{
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = $email_settings->smtp_user;         // SMTP username
		$mail->Password = $email_settings->smtp_password;     // SMTP password
		if (isset($email_settings->smtp_secure))
			$mail->SMTPSecure= $email_settings->smtp_secure;
	}
	//$mail->SMTPSecure = 'tls';                          // Enable TLS encryption, `ssl` also accepted
	$mail->Port = $email_settings->smtp_port;             // TCP port to connect to

	$mail->setFrom($email_settings->smtp_from_email, $email_settings->smtp_from_name);
	$mail->addAddress($row_email_to_send->RecipientEmail, $row_email_to_send->ExtraParams->Receiver); // Add a recipient
	$mail->addReplyTo($email_settings->smtp_from_email, $email_settings->smtp_from_name);

	$mail->isHTML(false);                                  // Set email format to HTML

	$mail->Subject = $row_email_to_send->ExtraParams->Subject;
	$mail->Body    = $row_email_to_send->ExtraParams->Body;

	if (null!=$rows_attachment)
	{
		foreach ($rows_attachment as $row_attachment)
			$mail->addStringAttachment($row_attachment->Content,$row_attachment->FileName);
	}

	if (!$mail->send())
	{
		$txt_error= "Mailer Error: {$mail->ErrorInfo}";
		trace($txt_error);
		trace("SMTPAuth=$mail->SMTPAuth, Username=$mail->Username, mail->SMTPSecure=$mail->SMTPSecure, Port=$mail->Port");
		throw new Exception($txt_error);
	}

	return $mail->getSentMIMEMessage();
}