<?php

require_once '../assets/helpers/post.php';

class Base_crud
{
	private function ThrowEmptyImplementation($method_name)
	{
		$class_name= get_class($this);
		$this->exit_bad_request("unimplemented method $class_name::$method_name!");
	}
	function create($obj) { $this->ThrowEmptyImplementation('create'); }
	function read($id) { $this->ThrowEmptyImplementation('read'); }
	function update($id,$obj) { $this->ThrowEmptyImplementation('update'); }
	function delete($ids) { $this->ThrowEmptyImplementation('delete'); }

	function exit_bad_request($error_text)
	{
		header("HTTP/1.1 400 Bad Request");
		write_to_log($error_text);
		if (function_exists('write_to_log_auth_info'))
			write_to_log_auth_info();
		exit;
	}

	function check_MandatoryGET($name)
	{
		if (!isset($_GET[$name]))
			$this->exit_bad_request("skipped _GET['$name']");
	}

	function check_Object_mandatory($name)
	{
		if (!isset($_POST[$name]))
			$this->exit_bad_request("skipped _POST['$name']");
	}

	function check_Object_mandatory_string($name)
	{
		$this->check_Object_mandatory($name);
		if (!is_string($_POST[$name]))
			$this->exit_bad_request("not string _POST['$name']");
	}

	function check_Object_mandatory_visible_string($name)
	{
		$this->check_Object_mandatory_string($name);
		$txt= $_POST[$name];
		if (''==trim($txt))
			$this->exit_bad_request("bad visible string _POST['$name']=$txt");
	}

	function check_Object_mandatory_email($name)
	{
		$this->check_Object_mandatory_string($name);
		$txt= $_POST[$name];
		if (5>strlen($txt))
			$this->exit_bad_request("too short email _POST['$name']=$txt");
		if (false==strpos($txt,'@'))
			$this->exit_bad_request("email without @ _POST['$name']=$txt");
	}

	function process_cmd()
	{
		$this->check_MandatoryGET('cmd');
		$cmd= $_GET['cmd'];
		switch ($cmd)
		{
			case 'add':
				$this->create(safe_POST($_POST)); 
				break;
			case 'get':
				$this->check_MandatoryGET('id');
				$this->read($_GET['id']);
				break;
			case 'update':
				$this->check_MandatoryGET('id');
				$object= safe_POST($_POST);
				$this->update($_GET['id'],$object);
				break;
			case 'delete':
				$this->check_MandatoryGET('id');
				$this->delete(explode(',',$_GET['id']));
				break;
			default:
				$this->exit_bad_request("unknown cmd \"$cmd\"");
		}
	}
}